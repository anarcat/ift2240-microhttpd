/****************************-*-C-*-***********************************
 * $Id: writer.h,v 2.5 2000-04-12 11:08:21-04 spidey Exp spidey $
 **********************************************************************
 * Proc�dures d'�criture HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef _WRITER_H
#define _WRITER_H

#include <sys/stat.h>

/***********************************************************************
 * Protocole HTTP/1.1, �criture
 *
 * En g�n�ral, ces fonctions �crivent un message appropri�, un fichier
 * ou le listing d'un r�pertoire sur le descripteur donn�.
 **********************************************************************/

/***********************************************************************
 * �crit un header HTTP de r�ponse avec code (200, 404..) sur le
 * descripteur fd
 * 
 * Si la valeur de httpCode est inconnue ou hors limite, on fait un 500
 *
 * On peut passer en param�tre un struct stat (voir stat(2)) pour avoir
 * header plus complet.
 ***********************************************************************/
int writeHttpHeader(int clientfd,
                    char *path,
                    int httpCode,
                    struct stat* fstats);

/***********************************************************************
 * Imprime une page HTML sur le descripteur donn�.
 *
 * On peut sp�cifier un titre NE CONTENANT PAS DE HTML, et un body
 * pouvant contenir du html. Il est important de noter que le 'body'
 * sera ins�r� entre 2 series de tags "<body>" - "</body></html>" pour
 * �viter les erreurs HTML.
 *
 * Si title est NULL, "" est �crit comme title, et si body est NULL, une
 * page vide est envoy�e seulement si title est NULL. Sinon le title est
 * reproduit dans le body.
 **********************************************************************/
int writeHtmlPage(int clientfd, char* title, char* body);

/***********************************************************************
 * Imprime une le flux d�crit pas fd sur clientfd
 *
 * read() des tampons de taille BUFSIZ sur fd et write() sur clientfd.
 * Retourne le nombres d'octets �crits sur clientfd ou -1 si une erreur
 * s'est produite.
 *
 * Ceci sera en g�n�ral utilis� pour des requ�tes GET.
 **********************************************************************/
int writeStream(int clientfd, int fd);

/***********************************************************************
 * �crit le fichier donn� par path sur clientfd
 *
 * Appelle writeStream() apr�s avoir ouvert le fichier.
 *
 * Retourne -1 si writeStream() �choue, -2 si le fichier n'a pas pu
 * �tre ouvert, 0 sinon.
 **********************************************************************/
int writeFile(int clientfd, char *path);

/***********************************************************************
 * �crit le contenu du r�pertoire sur clientfd
 **********************************************************************/
int writeDir(int clientfd, char* path);

/***********************************************************************
 * �xecute le script point� par path et redirige la sortie vers clientfd
 **********************************************************************/
int writeCGI(int clientfd, char* url, char *path, char* query_string);

/***********************************************************************
 * �crit une r�ponse appropri� au code HTTP sur clientfd
 *
 * Attention, certains codes ne sont pas implant�s et imprimeront un 500
 * si appel�s.
 * Les explications des codes proviennent du RFC 2616.
 **********************************************************************/
int writeCode(int clientfd, int code);

/***********************************************************************
 * �crit une r�ponse appropri� au code HTTP sur clientfd, header et page
 *
 * Appelle writeHttpHeader() writeCode()
 **********************************************************************/
int writeErrorCode(int clientfd, int code);


#ifdef HOSTFILE
/***********************************************************************
 * �crit les stats sur le serveur sur le fichier HOSTFILE
 **********************************************************************/
void writeHostfile(char* hostname, int port);
#endif

#endif
