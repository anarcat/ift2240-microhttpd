/* -*-C-*- */

/**
 * Ces macros d�terminent les translations URL->path du serveur.
 **/
#define HT_DIR "HTML/" /* r�pertoire de donn�es */
#define USE_HOME /* HT_DIR est relatif � $HOME */

/**
 * O� le serveur doit �crire son HOST:PORT
 *
 * Pas �crit si ind�fini
 *
 * fichier relatif au HT_DIR
 **/
/* #define HOSTFILE "2240_Serveur" */

/**
 * Le fichier �tant lu comme l'index des r�pertoires
 *
 * D�sactivez cette macros pour d�sactiver le 'feature'
 **/
#define INDEX_FILE "index.html" /* le fichier d'index */

/**
 * Si on fait un fork � chaque nouvelle connection
 **/
#define USE_FORK

/**
 * Si un STOP au terminal arr�te le serveur
 **/
/* #define STOP */

/**
 * Si on utilise les fichiers utils.c et inetsock.c
 **/
/* #define HAVE_UTILS_H */
/* #define HAVE_INETSOCK_H */

/**
 * from: FreeBSD: src/etc/services,v 1.55.2.6 1999/08/29 14:19:01:
 * The Well Known Ports are those from 0 through 1023.
 * The Registered Ports are those from 1024 through 49151
 * The Dynamic and/or Private Ports are those from 49152 through 65535
 */
#define DEFAULT_PORT 50080      /* le port par d�faut (port priv�) */
#define PENDING_CON 5           /* le nombre de connections 'en attente'
                                   que l'on prend */
