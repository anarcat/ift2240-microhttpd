////////////////////////////////////////////////////////////////////////
// Client graphique HTTP/0.9
////////////////////////////////////////////////////////////////////////
// @author The AnarCat
// @version $Id: Client.java,v 0.8 2000-04-12 17:53:33-04 spidey Exp spidey $
////////////////////////////////////////////////////////////////////////

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

////////////////////////////////////////////////////////////////////////
// Definition du client HTTP
////////////////////////////////////////////////////////////////////////  
class Client extends Frame {
  
  private TextArea output;
  private ServerList servList;
  private TextField url;
  private Button send, quit;
  private Checkbox test;
  private Label messages;
    
    //////////////////////////
    // Constructeur de base //
    //////////////////////////
  public Client () {

    super("Client HTTP/0.9");

    setupDisplay();
    setupHandlers();

  }

  ///////////////////////////////////////
  // Construit le display et l'affiche //
  ///////////////////////////////////////
  private void setupDisplay() {

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    //         setLayout(gridbag);
        
    setLayout(gridbag);
    // toutes les parties de la section du haut: 
    servList = new ServerList(); // liste de serveurs
    Label serLbl = new Label("Serveur"); // duh
    messages = new Label("Welcome!"); // messages du programme � l'usager
    Label pathLbl = new Label("Path:"); // duh.
    url = new TextField("", 30); // l'url � essayer
    output = new TextArea(10, 30);
    output.setEditable(false);
    send = new Button("Send"); // 3 boutons 
    quit = new Button("Quit"); // un au dessus de l'autre
    test = new Checkbox("Test");
        
    ///////////////////////////////////
    // Section superieure
           
    // on place le tout
    c.weighty = 1.0;
    c.weightx = 1.0;

    c.fill = GridBagConstraints.HORIZONTAL;
    gridbag.setConstraints(serLbl, c);
    add(serLbl);         // serveur

    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(messages, c);
    add(messages);       // message termine la ligne
        
    c.gridwidth = 1;
    c.fill = GridBagConstraints.BOTH;
    gridbag.setConstraints(servList, c);
    add(servList);       // serveur � gauche fill vert+horiz

    c.fill = GridBagConstraints.NONE;
    gridbag.setConstraints(pathLbl, c);
    add(pathLbl);        // label normal 
        
    c.gridwidth = GridBagConstraints.REMAINDER;
    c.fill = GridBagConstraints.HORIZONTAL;
    gridbag.setConstraints(url, c); 
    add(url);            // url termine la ligne avec fill � droite
                
    // fin de la section superieure
    ///////////////////////////////////

    ////////////////////////////////
    // Section de gauche (Centre)
    c.gridwidth = 1;
    c.gridheight = GridBagConstraints.REMAINDER;
    c.fill = GridBagConstraints.BOTH;
    gridbag.setConstraints(output, c); 
    add(output);    // partie gauche
    // fin section de gauche (centre)
    /////////////////////////////////

    /////////////////////////////////
    // Section de droite
    c.fill = GridBagConstraints.NONE; // on ne change pas leur forme
    c.gridheight = 1;
    c.gridwidth = GridBagConstraints.REMAINDER; // 3 lignes
        
    gridbag.setConstraints(send, c);
    gridbag.setConstraints(quit, c);
    gridbag.setConstraints(test, c);
        
    add(send);
    add(quit);
    add(test);
    // fin section de droite
    ////////////////////////////////////

    setSize(450, 300);
    show();
        
  }

  ////////////////////////////////////////////////////////////////////
  // Active des adapters pour reconna�tres les events des objets de la
  // fen�tre (boutons, fermeture, etc).
  ////////////////////////////////////////////////////////////////////
  private void setupHandlers() {

    quit.addMouseListener(new MouseAdapter() {
        public void mouseReleased(MouseEvent e) {quit(0);}
      });
    
    send.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                HttpRequest req = null;
                String buffer;
                
                send.setEnabled(false);
                messages.setText("Opening connection to server...");
                try {
                    req = new HttpRequest
                        (servList.getHost(), servList.getPort());
                } catch (IOException exc) {
                    messages.setText("Can't open socket to host " +
                                     servList.getHost() + ":" +
                                     servList.getPort()+ " : " +
                                     exc.getMessage());
                    send.setEnabled(true);
                    return;
                }

                messages.setText("Getting ressource...");
                output.setText("");

                try {
                    buffer = req.get("/" + url.getText(), test.getState());

                    output.append(buffer);
                    
                    messages.setText("Done getting resource");
                
                    req.close();
                    
                } catch (IOException exc) {
                    System.out.println("IO Error from host "
                                       + exc.getMessage());
                } finally {
                    send.setEnabled(true);
                }
                
            }
        });
        
    addWindowListener(new WindowAdapter() {
        public void windowOpened(WindowEvent e) {}
        public void windowClosing(WindowEvent e) {quit(0);}
        public void windowClosed(WindowEvent e) {}
        public void windowIconified(WindowEvent e) {}
        public void windowDeiconified(WindowEvent e) {}
        public void windowActivated(WindowEvent e) {}
        public void windowDeactivated(WindowEvent e) {}
      });
        
  }

  ///////////////////////////////////////////////////////////////
  // Quite le programme et fait le cleanup (rien � faire... :) //
  ///////////////////////////////////////////////////////////////
  private void quit(int code) {System.exit(code);}
  
  public static void main(String args[]) {new Client();}
  
}
