/****************************-*-C-*-***********************************
 * $Id: parser.h,v 2.1 2000-04-12 04:49:53-04 spidey Exp spidey $
 **********************************************************************
 * Parser HTTP/1.1, declarations
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef _PARSER_H
#define _PARSER_H

/***********************************************************************
 * Parser
 *
 * RFC 2616 dit que:
 *
 * Request-Line = Method SP Request-URI SP HTTP-Version CRLF
 **********************************************************************/

/***********************************************************************
 * Retourne la m�thode HTTP d'une cha�ne
 *
 * Les m�thodes HTTP/1.1 sont, d'apr�s RFC 2616:
 * 
 * Method  = "OPTIONS" | "GET" | "HEAD" | "POST" | "PUT" | "DELETE" |
 *           "TRACE" | "CONNECT"
 *
 * Il y a �galement la m�thode sp�ciale et optionelle "STOP"
 * 
 * Retourne une macro d�signant la m�thode. Retourne HTTP_UNKNOWN si 
 * la m�thode est inconnue pour la version HTTP courante ou HTTP_ERROR
 * si buffer est NULL. 
 ***********************************************************************/
int parseMethod(char* buffer);

/************************************************************************
 * Retourne le fichier sans le slash ou NULL s'il n'y a pas de slash au
 * d�but de la cha�ne.
 ***********************************************************************/
char* parseUrl(char* buffer);

/************************************************************************
 * Interpr�te la cha�ne donn�e comme une version HTTP
 *
 * D'apr�s RFC 2616: HTTP-Version = "HTTP" "/" 1*DIGIT "." 1*DIGIT
 ***********************************************************************/
int parseVersion(char* buffer);

/***********************************************************************
 * Ces fonctions retournent un token issu du buffer
 **********************************************************************/
char* nextToken(char** buffer);

#endif
