
---------------------------------------------------------------------------
                                                                           
         IFT2240, d�mo # 11: Compilation, librairies et Makefile.          
                                                                           
---------------------------------------------------------------------------

�tapes de compilation en C.
   
 1. Passer le pr�-processeur de compilation qui traite toutes les commandes
    qui commencent par un "#" (#define, #ifdef, #ifndef, #else, #endif, #
    include).
 2. G�n�ration des fichiers objets (".o") � partir des fichiers sources
    (".c").
 3. Appel � la commande ld pour faire le "linkage" des fichiers objets vers
    un fichier ex�cutable (binaire).  Cet appel est transparent, car il est
    fait par gcc.

Options du compilateur gcc.
   
  *  -c: Sp�cifie de ne g�n�rer que les fichiers objets (".o") et de ne
    pas faire l'�tape de "linkage".
  *  : Sp�cifie une librairie � utilser lors du linkage.  Les
    librairies sont des fichiers de type lib<x>.a ou lib<x>.so o� <x> est
    le nom de la librairie.  Les librairies se trouvent habituellement dans
    le r�pertoire "/usr/lib/", mais on peut les mettre ailleurs si on
    utilise le parem�tre "-L".
  *  : Sp�cifie les r�pertoires suppl�mentaires <x> o� chercher des
    fichiers " .h".  Le r�pertoire par d�faut contient "/usr/include/" et
    ".".
  *  : Sp�cifie les r�pertoires suppl�mentaires <x> o� chercher des
    fichiers de librairie.  Le r�pertoire par d�faut contient "/usr/lib/",
    mais PAS ".".

Exemples: Exemple1.c
   
  *  Compiler ce programme en ne g�n�rant que le fichier objet ("gcc -c
    Exemple1.c").
  *  Essayer de "linker" le fichier objet en faisant "gcc Exemple1.o". 
    Vous verrez qu'il est impossible de le faire.  � la place, utiliser
    "gcc Exemple1.o -lm", car ce programme utilise la fonction "sin" qui
    est d�finie dans la librairie "m" (librairie math�matique).

Librairies (Archive ".a").

Une librairie est un regroupement de fichiers objets (".o").  Apr�s avoir
compil� plusieurs fichiers vers du code objet, on peut regrouper un ou
plusieurs fichiers objets dans une librairie.  Pour g�n�rer une librairie,
on utilise la commande ar (archive).  La commande ar a plusieurs options,
mais les des seules qui nous int�ressent sont "-r" et "-t".
   
  *  : Cr�e la librairie <nom
    de lib> qui contiendra les fichiers objets pass�s en param�tre.
  *  : Affiche la liste des fichiers objets dans la
    librairie.

Note: Pour �tre utilisable, le nom de fichier d'une librairie doit
commencer par "lib" et se terminer par ".a".

Librairies partag�es (Shared Librairies ".so").

Une librairie partag�e (Shared Librairy en anglais) resemble beaucoup � une
librairie ordinaire, sauf qu'elle n'est pas inclue dans le binaire du
programme � la compilation.  Elle est seulement charg�e en m�moire lors de
l'execution du programme.  Cela r�sulte en des binaires beaucoup plus
petits, donc en une �connomie d'espace disque (cette �connomie peut m�me
�tre assez consid�rable).

Par d�faut, quand on utilise le param�tre "-l" de gcc, le compilateur
cherche pour les librairies partag�es en premier et si elle n'existe pas,
il utilise les librairies ordinaires.  Donc l'exemple "Exemple1.c" avec la
librairie math�matique a �t� "link�" avec le fichier "libm.so" et non
"libm.a".  Si on veut plutot "linker" avec la librairie statique (".a"), il
faut utiliser le param�tre "-static" de la commande gcc.  La ligne de
compilation statique de "Exemple1.c" devient donc: "gcc Exemple1.o -lm
-static".

Exercice: "Linker" "Exemple1.o" avec et sans librairie partag�e.  Quel est
la diff�rence de taille entre les programmes ?

Pour cr�er une librairie partag�e, on utilise la commande ld avec le
param�tre "-shared".
   
  *  : Cr�e la
    librairie partag�e <nom de lib> qui contiendra les fichiers objets
    pass�s en param�tre.

Note: Pour �tre utilisable, le nom de fichier d'une librairie partag�e doit
commencer par "lib" et se terminer par ".so".

Si on veut compiler un programme avec une librairie partag�e que nous avons
cr��e, il faut soit mettre la librairie partag�e dans le r�pertoire "/usr/
lib" (ce qui est impossible sur le r�seau IRO) ou il faut indiquer au
programme o� il doit trouver la librairie partag�e.  Pour indiquer le lieu
d'une librairie partag�e, il faut ajouter � la fin de la ligne de
compilation "-Xlinker -rpath -Xlinker <path o� trouver la librairie
partag�e>".

Comment combiner des modules.

Jusqu'� maintenant, nous avons combin� des modules en faisant des "#
include" de fichier ".c".  Cette m�thode, bien qu'elle fonctionne, n'est
pas id�ale.  En g�n�rale, on fait des "#include" de fichiers ".h" et non
".c".

Si on veut combiner plusieurs modules pour g�n�rer un programme, nous
pouvons utiliser plusieurs m�thodes.  Nous allons pr�senter ces m�thodes
avec la solution du travail pratique num�ro 8.  Pour pouvoir suivre les
exemples, vous aurez besoins des fichiers suivant: inetsock.h inetsock.c
utils.h utils.c microserve1.c microserve2.c.
   
  *  gcc microserve1.c: Code avec des #include "qqch.c".  Pas tr�s beau,.
    mais �a marche.  Cette m�thode suppose que inetsock.h, inetsock.c,
    utils.h, utils.c et microserve1.c sont dans le r�pertoire courant.
  *  gcc microserve2.c inetsock.c utils.c: Les #include "qqch.c" ont �t�
    remplac�s par des #include "qqch.h".  Il faut donc donner au
    compilateur l'endroit o� sont d�finies les fonctions contenues dans les
    ".h".  Cette m�thode de compilation est beaucoup plus belle que la
    pr�c�dente, mais on peut encore faire mieux.  Cette m�thode suppose
    encore que inetsock.h, inetsock.c, utils.h, utils.c et microserve2.c
    sont dans le r�pertoire courant.
  *  gcc microserve2.c inetsock.o utils.o: Encore avec de #include
    "qqch.h", mais on donne des fichiers objets en param�tre � la place des
    fichiers sources.  L'avantage de cette m�thode est que la compilation
    de inetsock.c vers inetsock.o et de utils.c vers utils.o n'est pas
    refaite � chaque compilation de microserve2.c.  Dans le cas d'un tr�s
    gros projet avec plusieurs dizaine de fichiers sources, on gagne
    beaucoup de temps � ne recompiler que le minimum de fichiers sources. 
    Cette methode suppose que inetsock.h, utils.h, et microserve2.c sont
    dans le r�pertoire courant et que inetsock.c et utils.c ont �t�
    pr�c�demment compil�s vers les fichiers objets.
  *  gcc microserve2.c -L. -lmicroserve: Ressemble beaucoup � la m�thode
    pr�c�dente, sauf que les deux ".o" on �t� mis dans une librairie
    statique.  Cette m�thode suppose que inetsock.h, utils.h, et
    microserve2.c sont dans le r�pertoire courant et que inetsock.c et
    utils.c ont �t� pr�c�demment compil�s vers les fichiers objets et mis
    dans la librairie "libmicroserve.a".
  *  gcc microserve2.c -L. -lmicroserveso -Xlinker -rpath -Xlinker .:
    Ressemble beaucoup � la m�thode pr�c�dente, sauf que les deux ".o" ont
    �t� mis dans une librairie partag�.  Nous avons donc un programme plus
    petit.  Cette m�thode suppose que inetsock.h, utils.h, et microserve2.c
    sont dans le r�pertoire courant et que inetsock.c et utils.c ont �t�
    pr�c�demment compil�s vers les fichiers objets et mis dans la librairie
    partag�e "libmicroserveso.so".
  *  gcc microserve2.c -I~dift2240/HTML/H00/Demos/Demo11/ -L~dift2240/
    HTML/H00/Demos/Demo11/ -lmicroserve: Cette fois ci, il ne faut que le
    fichier microserve2.c dans le r�pertoire courant.  Tous les autres
    fichiers (".h" et librairies) sont pris du r�pertoire ~dift2240/HTML/
    H00/Demos/Demo11/.
  *  gcc microserve2.c -I~dift2240/HTML/H00/Demos/Demo11/ -L~dift2240/
    HTML/H00/Demos/Demo11/ -lmicroserveso -Xlinker -rpath -Xlinker
    ~dift2240/HTML/H00/Demos/Demo11/: M�me chose que la m�thode pr�c�dente,
    sauf avec la librairie partag�e.

Note: Les fichiers "inetsock.h" et "utils.h" contiennent du code bizarre du
type:

#ifndef _INETSOCK_H_

#  define _INETSOCK_H_

   /* ... */

#endif

#ifndef _UTILS_H_

#  define _UTILS_H_

   /* ... */

#endif

Cela est pour �viter une double d�finition des fonctions si on inclut deux
fois le m�me fichier ".h".  Cette double d�finition causeraient une erreur
� la compilation.

Commande Shell.

Pour voir les symboles d'un fichier objet, d'une librairie ou d'un binaire,
vous pouvez utiliser la commande nm.  Pour avoir plus de d�tails sur cette
commande, utiliser la commande man.

Makfiles.

Fonctionnement des Makefiles.

Un makefile est une sorte de script interpr�t� par la commande make.  Un
makefile repr�sente tr�s bien les d�pendances entre un ensemple de t�che et
permet d'�viter d'ex�cuter une t�che si cette t�che n'est pas n�c�ssaire.

Voici un makefile avec deux t�ches et o� la t�che #1 doit �tre ex�cut�e
apr�s la t�che #2 (makefile1):

tache1: tache2
<tab>echo Tache1

tache2:
<tab>echo Tache2

Ce makefile sp�cifie deux t�ches: tache1 et tache2.  Chaque t�che commence
au d�but d'une ligne et se termine par un deux-points (":").  Apr�s le
deux-points, on peut donner une liste des t�ches � ex�cuter avant
d'ex�cuter la t�che courante (dans ce cas, tache2 doit �tre ex�cut� avant
tache1).  Les commandes � ex�cuter pour une t�che sont donn�es apr�s le nom
de la t�che, une commande par ligne et la commande pr�c�d�e d'un caract�re
de tabulation (<tab>).

Pour utiliser un makefile, il faut faire la commande make.  La commande
make ex�cute la premi�re t�che du fichier "makefile" ou "Makefile" dans le
r�pertoire courant.  Si on veut sp�cifier un fichier makefile diff�rent, on
peut utiliser le param�tre "-f" ("make -f makefile1" ex�cutera la premi�re
t�che du fichier makefile "makefile1").  On peut aussi sp�cifier une t�che
diff�rente de la premi�re en donnant le nom de la t�che ("make -f makefile1
tache2" ex�cutera la t�che "tache2" du fichier makefile "makefile1").

Voici un autre makefile (makefile2):

tache1: tache2
<tab>echo tache1 > tache1

tache2: tache3
<tab>echo tache2 > tache2

tache3:
<tab>echo tache3 > tache3

Ce makefile cr�e des fichiers.  La permi�re fois que l'on ex�cute ce
makefile, on cr�e les fichiers tache1, tache2 et tache3, mais la deuxi�me
fois, on a le message "make: `tache1' is up to date.".  Cela est le
m�canisme qui �vite de faire une t�che si cette derni�re n'a pas besoins
d'�tre faite.  Si le fichier portant le nom d'une t�che est plus r�cent que
tous les fichiers dont cette t�che d�pend, on n'ex�cute pas cette t�che.

Exercice: Apr�s avoir ex�cut� deux fois ce makefile, modifier le fichier
tache3 et refaite make.  Faite la m�me chose avec le fichier tache2.  Que
ce passe-t-il ?

Voici deux autres exemples de makefile, mais ils sont facile � comprendre,
donc nous ne les expliquerons pas: makefile3 et makefile4.

Pour mettre des commentaires dans un makfile, on utilise "#" qui signifie
que le reste de la ligne est des commentaires.

Utilit� des Makefiles.

Pour faciliter le travail de compilation, on peut �crire le makefile
suivant pour compiler "microserve2.c": makefilemicroserve1.  Ce makefile ne
compilera que ce qui est n�c�ssaire en fonction des modifications qui ont
�t� faites dans les fichiers.  Faites des tests en modifiant des fichiers
sources (en ajoutant des espaces par exemple).

Les Makefiles plus en d�tails.

Dans un makefile, on peut utiliser des variables d'environnement.  Pour
utiliser la variable d'environnement VAR, on fait $(VAR).  En plus des
variables d'environnement, on peut utiliser des variables locales d�finies
dans le makefile.  Voici un exemple (makefile5):

MY_VAR = "Ma variable."

all:
        echo $(HOME)
        echo $(MY_VAR)

Il existe plusieurs variables qui sont commun�ment utilis�es par make.
Elles sont:
   
  *  CC: Commande � utiliser pour compiler (par exemple: cc, gcc ou g++).
  *  CFLAGS: Les param�tres � passer � la compilation (-Wall, -I~dift2240
    /...).
  *  LDFLAGS: Les param�tres � passer au linkage (-L~dift2240/..., -lm).

Il existe aussi des variables particuli�res lors d'une t�che.  Elles sont:
   
  *  $@: Nom de la cible.
  *  $<: Nom du fichier pr�alable.
  *  $?: Liste des pr�alables plus r�cents que la cible.

Il est possible de substituer des cha�nes de caract�re dans une variable. 
"${VAR:<Source>=<Destination>}" remplacera toutes les occurences de <Source
> par <Destination>.  Par exemple (makefile6):

VAR = a.c b.c d.e

all:
<tab>echo ${VAR:.c=.o}

On peut cr�er des r�gles g�n�rales pour g�n�rer un fichier vers un autre. 
Voici des exemples:

.SUFFIXES: .c .o .class .java

.c.o:
<tab>$(CC) $(CFLAGS) -c $<

.c:
<tab>$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

.java.class:
<tab>javac $<

La ligne commen�ant par ".SUFFIXES" indique � make les nouvelles extensions
� conna�tre.  La premi�re r�gle indique comment g�n�rer un ".o" � partif
d'un ".c" et la seconde, comment g�n�rer un binaire (pas n'extension) �
partir d'un ".c", et la derni�re, comment g�n�rer un ".class" � partir
d'une ".java".

Avec ces r�gles pr�d�finies, il devient tr�s simple d'�crire un makefile
pour g�n�rer "microserve2" � partir des fichiers (makefilemicroserve2).
Un makefile pour g�n�rer "microserve2" � partir de la librairie statique
est encore plus cours (makefilemicroserve3).

---------------------------------------------------------------------------
Date de derni�re modification: 2000-04-03
Auteur: Jean-Fran�ois Gagn�
---------------------------------------------------------------------------
