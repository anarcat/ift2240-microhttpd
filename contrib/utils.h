/*****************************************************************************
 *
 * Nom du fichier: utils.h
 *
 * Auteur: Jean-Francois Gagne (e-mail: gagnjea@iro.umontreal.ca)
 * Date  : 2000-03-17
 *
 * Description: Fonctions utiles pour l'implantation du projet du cours IFT2240
 *
                session hiver 2000.
 *
 * Version: 1.0
 *
 * Modifi� par: -N/A-
 * Date       : -N/A-
 *
 *****************************************************************************/
 
#ifndef _UTILS_H_

   #define _UTILS_H_

   int DoFileExists(char *strFile);
   /* Retourne 1 si le fichier passe en parametre existe, 0 sinon. */


   int IsFileDangerous(char *strFile);
   /* Retourne 1 si le nom de fichier passe en parametre pourrait etre dangereux,
    *    0 sinon.  Un nom de fichier dangereux est un nom de fichier qui contient
    *    la sous-chaine "..". */


   int IsFileDirectory(char *strFile);
   /* Retourne 1 si le fichier passe en parametre est un repertoire, 0 sinon. */

#endif

/**************************/
/* Fin du fichier utils.h */
/**************************/
