/* Pour documentation, voir dans inetsock.h */

/*****************************************************************************
 * Fichier: inetsock.c
 *
 * Description: Implantation des fonctions de inetsock.h.
 *
 *****************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "inetsock.h"


/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
/*                       STREAMS                            */
/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */

int internet_connect(char *host, int port)
{
    int         res,    fd;
    struct sockaddr_in  sa;
    
    fd = socket (AF_INET, SOCK_STREAM, 0) ;
    if (fd < 0) {
       perror("internet_connect: socket");
       exit(1);
    }

    printf("%s:%d\n", host, port );
    make_sockadr( &sa, host_aton(host), port) ;
    res = connect(fd, (struct sockaddr *) &sa, sizeof(sa));
    if (res < 0) {
        close(fd);
        perror("internet_connect: connect");
        exit(1);
    }
    return(fd);
} 
        

int internet_server (int Port)
{
    struct sockaddr_in sa;
    int    fd;
    
    fd = socket (AF_INET, SOCK_STREAM, 0) ;
    if (fd < 0) {
       perror("internet_server: socket");
       exit(1);
    }

    make_sockadr( &sa, INADDR_ANY, Port) ;
    if ( bind(fd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
        close(fd);
        perror("internet_server: connect");
        exit(1);
    }

    listen(fd, 5); /* max # of queued connects */
    return(fd);
} 


int internet_accept(int fd, struct sockaddr_in  *cli_adr)
{
    int     newfd;
    int     dummy ;
    
    newfd = accept( fd, (struct sockaddr *) cli_adr, & dummy);
    if (newfd < 0) {
        perror("internet_accept");
        exit(1);
    }
    return(newfd);
} 


int stream_write(int fd, char *ptr, int n)
{
    int  res;
    
    while (n > 0)  {
        if ((res = write(fd, ptr, n)) <= 0)  return (res);          
        n   -= res;
        ptr += res;
    }
    return (0);
} 


int stream_read(int fd, char * ptr, int nbytes)
{
    int  n, res;
    
    n = nbytes;
    while (n > 0)  {
        if ((res = read(fd, ptr, n)) < 0)  return (res);
        if (res == 0)
            break;
        n    -= res;
        ptr  += res;
    }
    return (nbytes - n);
} 


int read_char(int fd)
{
    char c;
    
    if ( read( fd, &c, 1) == 1) 
        return(c) ; 
    else
        return( -1 );
} 

    
int read_string(int fd, char *ptr, int maxlen, char C)
{
    int  n, nread;
    char c;
    
    for (n=1; n < maxlen; n++)  {
        if ( (nread = read(fd, &c, 1)) == 1) {
            *ptr++ = c;
            if (c == C)  break;  
        }
        else if (nread == 0 && n>0)  
            break;
        else
            return (nread);
    }
    *ptr = '\0';
    return(n) ;
} 



/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
/*                       DATAGRAMS                          */
/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */

int datagram_socket()
{
    int                fd ;
    struct sockaddr_in sock_adr ;

    fd = socket (AF_INET, SOCK_DGRAM, 0) ;
    if (fd < 0) {
       perror("datagram_socket: socket");
       exit(1);
    }
    
    make_sockadr( &sock_adr, INADDR_ANY, 0);    
    if ( bind( fd, (struct sockaddr *) &sock_adr, sizeof(sock_adr)) < 0) {
        close( fd );
        perror("datagram_socket: bind");
        exit(1);
    }
    return (fd) ;
}  


int send_datagram(int fd, char *buff, int n, struct sockaddr_in *DestAdr)
{       
    return (sendto (fd, buff, n, 0, (struct sockaddr *) DestAdr, sizeof(*DestAdr)) );     
} 


int receive_datagram(int fd, char *buff, int n, struct sockaddr_in *PeerAdr)
{   
    int  res, Len ;
    
    res = recvfrom (fd, buff, n-1, 0,
        (struct sockaddr *) PeerAdr, &Len)  ;

    if (res >= 0 )   buff[res] = '\0' ;  ;
    return(res);
} 
 


/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
/*            NETWORK ADDRESSING FUNCTIONS                  */
/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */

static char hostName[PATHSIZE];
char * host_name()
{           
    gethostname (hostName, PATHSIZE);
    return ( hostName );
}


int32_t host_aton(const char *name)
{
    struct hostent  *hostStruct;
    struct in_addr  *hostNode;

    if ( name == NULL || strcmp (name, "") == 0)
        gethostname (hostName, PATHSIZE);
    else
        strcpy (hostName, name);

    hostStruct = gethostbyname ( hostName);
    if (hostStruct == NULL)
        return (unsigned long)(NULL);

    hostNode = (struct in_addr *) hostStruct->h_addr;
    return (hostNode->s_addr);
}


char *host_ntoa( u_long Host )
{   
    return( gethostbyaddr((char *)&Host, sizeof(u_long), AF_INET) -> h_name);
}


char *host_ntod(u_long Host)
{    struct in_addr in_a;

     in_a.s_addr = Host;
     return inet_ntoa(in_a);
}


void make_sockadr(struct sockaddr_in *SockAdr, u_long Host, int Port)
{
    memset( SockAdr, 0, sizeof(*SockAdr) );  /* Probably unecessary  */
    SockAdr->sin_family      = AF_INET ;
    SockAdr->sin_addr.s_addr = Host;
    SockAdr->sin_port        = htons(Port) ;
} 


void split_sockadr(struct sockaddr_in *SockAdr, u_long  *Host, int *Port)
{   
    *Port = ntohs(SockAdr->sin_port) ;
    *Host = ntohl(SockAdr->sin_addr.s_addr );
}


int socketport(int fd)
{
    struct sockaddr_in SockAdr;
    int                n;
    
    n = sizeof(SockAdr);
    if (getsockname (fd, (struct sockaddr *) &SockAdr, &n) < 0)  
    {  
       perror("socket_to_HostPort");
       exit(1);
    }

    return (ntohs(SockAdr.sin_port)) ;
} 


int serviceport (char *service, char *protocol )
{
    struct servent *serv;
   
    if ( protocol == NULL || strcmp (protocol, "") == 0)
         protocol = "tcp";

    serv = getservbyname( service, protocol);
    if ( serv == NULL ) {
        fprintf(stderr,"servicePort: %s service not found %s.\n",
                  protocol, service);
        fflush(stderr);
        exit(1);
    } 
              
    return (serv->s_port) ;
} 


int sockadr_size()
{  
    return sizeof( struct sockaddr_in);
} 


/*****************************/
/* Fin du fichier inetsock.c */
/*****************************/
