/*****************************************************************************
 *
 * Nom du fichier: microserve.c
 *
 * Auteur: Jean-Francois Gagne (e-mail: gagnjea@iro.umontreal.ca)
 * Date  : 2000-03-16
 *
 * Description: Implantation d'un serveur web minimal.
 *
 * Version: 1.0
 *
 * Modifi� par: -N/A-
 * Date       : -N/A-
 *
 *****************************************************************************/
 
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "inetsock.c"
#include "utils.c"


#define HOST_STRING_LENGTH 200
#define DEFAULT_SOCKET 10024

#define CONTENT_TYPE_TEXT_HTML  "Content-Type: text/html\r\n"
#define CRLF "\r\n"
#define CODE_400_TEXT "<HTML>\r\n<HEAD>\r\n   <TITLE>Bad request</TITLE>\r\n</HEAD>\r\n\r\n<BODY>\r\n<H1>400 Bad Request</H1>\r\n<P>A bad request was sent to the web server..</P>\r\n</BODY>\r\n\r\n</HTML>\r\n"
#define CODE_403_TEXT "<HTML>\r\n<HEAD>\r\n   <TITLE>Forbiden</TITLE>\r\n</HEAD>\r\n\r\n<BODY>\r\n<H1>403 Forbiden</H1>\r\n<P>Ha Ha! You try to get something you were not supposed to.  Well, you are busted.</P>\r\n</BODY>\r\n\r\n</HTML>\r\n"
#define CODE_404_TEXT "<HTML>\r\n<HEAD>\r\n   <TITLE>Not Found</TITLE>\r\n</HEAD>\r\n\r\n<BODY>\r\n<H1>404 Not Found</H1>\r\n<P>The requested object was not found.</P>\r\n</BODY>\r\n\r\n</HTML>\r\n"
#define ERROR_CODE_200 "HTTP/1.0 200 Ok\r\n"          /* Ok. */
#define ERROR_CODE_400 "HTTP/1.0 400 Bad Request\r\n" /* Bad request. */
#define ERROR_CODE_403 "HTTP/1.0 403 Forbiden\r\n"    /* Forbiden. */
#define ERROR_CODE_404 "HTTP/1.0 404 Not Found\r\n"   /* Not found. */
#define SERVER "Server: Jean-Francois Gagne Web Server\r\n"


int main(int nArgs, char *strArgs[])
{
   char *strBufferDeClient, *strPathFichier, *strExtension, *strTemp, *strErrorCode, *strContentType, *strTxt;
   char strBufferToClient[BUFSIZ], strHost[HOST_STRING_LENGTH];
   int fdServeur, fdClient, fdClientFile, i, n, nDoubleCRLFPresent, nPort, nReadDeClient, nTailleBufferDeClient;

   /* Initialisation du port. */
   if (nArgs == 2)
   {
      nPort = atoi(strArgs[1]);
      if (nPort < 0) nPort = DEFAULT_SOCKET;
   }
   else
      nPort = DEFAULT_SOCKET;
   
   /* Creation, bind et listen du socket. */
   fdServeur = internet_server(nPort);
   if (fdServeur <= 0)
   {
      perror("Erreur de creation du socket");
      exit(-1);
   }
   gethostname(strHost, HOST_STRING_LENGTH);
   printf("Le serveur est sur le host %s.\n", strHost);
   printf("Le serveur ecoute sur le port #%d.\n", socketport(fdServeur));
   
   /* Boucle de traitement des requetes. */
   printf("Debut du traitement des requetes.\n\n");
   while(1)
   {
      /* Accepter une connection. */
      fdClient = accept(fdServeur, NULL, NULL);
      if (fdClient == -1)
      {
         perror("Erreur de accept");
         exit(-1);
      }
      
      /* Lecture du data d'une requete jusqu'au double <CRLF>. */
      nDoubleCRLFPresent = 1;
      nReadDeClient = 0;
      nTailleBufferDeClient = BUFSIZ;
      strBufferDeClient = (char *)malloc((nTailleBufferDeClient + 1) * sizeof(char)); /* + 1 pou le '\0'. */
      while(1)
      {
         /* Si on arrive a la fin du data avant le double CRLD, alors bad request. */
         if (read(fdClient, strBufferDeClient +nReadDeClient, 1) != 1)
         {
            nDoubleCRLFPresent = 0;
            break;
         }
         
         nReadDeClient++;
         
         /* Si on est au double <CRLF>, on arrete. */
         if (nReadDeClient > 4 && strncmp(strBufferDeClient + nReadDeClient - 4, "\r\n\r\n", 4) == 0)
            break;

         /* Si on depasse le buffer, on aloue un plus grand. */
         if (nReadDeClient == nTailleBufferDeClient)
         {
            nTailleBufferDeClient *= 2;
            strTemp = (char *)malloc((nTailleBufferDeClient + 1) * sizeof(char));
            strncpy(strTemp, strBufferDeClient, nReadDeClient);
            free(strBufferDeClient);
            strBufferDeClient = strTemp;
         }
      }
      strBufferDeClient[nReadDeClient] = '\0';

      /* Affichage de la requete. */
      printf("Reception d'une requete:\n%s\n", strBufferDeClient);
      
      /* Gestion du cas d'arret su serveur. */
      if (strncmp(strBufferDeClient, "STOP", 4) == 0)
      {
         printf("Reception d'une requete STOP, arret du serveur.\n");
         free(strBufferDeClient);
         close(fdClient);
         break;
      }
      
      /* Parsing de la requete. */
      if (strncasecmp(strBufferDeClient, "GET /", 5) != 0 || !nDoubleCRLFPresent)
      {
         strErrorCode   = ERROR_CODE_400;
         strContentType = CONTENT_TYPE_TEXT_HTML;
         strTxt         = CODE_400_TEXT;
      }
      else
      {
         strPathFichier = strBufferDeClient + 5;
         strTemp = strPathFichier;
         while (*strTemp != ' ')
            strTemp++;
         *strTemp = '\0';
         strTemp++;

         if (strncasecmp(strTemp, "HTTP/1.", 7))
         {
            strErrorCode   = ERROR_CODE_400;
            strContentType = CONTENT_TYPE_TEXT_HTML;
            strTxt         = CODE_400_TEXT;
         }
         else
         {
            /* Les fichiers qui contiennent des .. sont une tentativde de hacking. */
            if (IsFileDangerous(strPathFichier))
            {
               strErrorCode   = ERROR_CODE_403;
               strContentType = CONTENT_TYPE_TEXT_HTML;
               strTxt         = CODE_403_TEXT;
               fdClientFile = -1;
            }
            else
               fdClientFile = 0;
            
            if (!fdClientFile)
            {
               fdClientFile = open(strPathFichier, O_RDONLY);

               if (fdClientFile > 0)
               {
                  strErrorCode   = ERROR_CODE_200;
                  strContentType = CONTENT_TYPE_TEXT_HTML;
                  strTxt         = NULL;
               }
               else
               {
                  strErrorCode   = ERROR_CODE_404;
                  strContentType = CONTENT_TYPE_TEXT_HTML;
                  strTxt         = CODE_404_TEXT;
               }
            }
         }
      }
      
      write(fdClient, strErrorCode,   strlen(strErrorCode));
      write(fdClient, SERVER,         strlen(SERVER));
      write(fdClient, strContentType, strlen(strContentType));
      write(fdClient, CRLF,           strlen(CRLF));
      if (strTxt)
         write(fdClient, strTxt,      strlen(strTxt));
      else
      {  
         while((n = read(fdClientFile, strBufferToClient, BUFSIZ)) > 0)
            write(fdClient, strBufferToClient, n);
         close(fdClientFile);
      }

      free(strBufferDeClient);
      close(fdClient);
   }
   
   close(fdServeur);
   return 0;
}

/*******************************/
/* Fin du fichier microserve.c */
/*******************************/
