/*****************************************************************************
 *
 * Nom du fichier: utils.c
 *
 * Auteur: Jean-Francois Gagne (e-mail: gagnjea@iro.umontreal.ca)
 * Date  : 2000-04-06
 *
 * Description: Implantation des fonctions de utils.h.
 *
 * Version: 1.0
 *
 * Modifi� par: -N/A-
 * Date       : -N/A-
 *
 *****************************************************************************/
 
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "utils.h"
 
int DoFileExists(char *strFile)
{
   struct stat Info;

   if (stat(strFile, &Info) == 0)
         return 1;
   else
      return 0;
}

int IsFileDangerous(char *strFile)
{
   int i, nLength;
   
   nLength = strlen(strFile) - 1;

   if (*strFile == '/') return 1;

   for(i = 0; i < nLength; i++)
      if (strFile[i] == '.' && strFile[i+1] == '.')
         return 1;
   return 0;
}

int IsFileDirectory(char *strFile)
{
   struct stat Info;

   if (stat(strFile, &Info) == 0)
   {
      if (S_ISDIR(Info.st_mode))
         return 1;
      else
         return 0;
   }
   else
      return 0;
}

/**************************/
/* Fin du fichier utils.c */
/**************************/
