
/* =====================================================
 *  INETSOCK.H  (v1.4)
 * ========================
 *
 *  C routines for INTERNET SOCKET programming
 *     (previously known as "tcpsock.c")
 *
 *  - initiallement crees pour package Simula: SOCKET.SIM
 *   - Version 1.5  (mars 2000)
 *        Correction de certains headers.
 *   - Version 1.4  (nov  1997: J. Vaucher)
 *        Fonction utilitaires ajoutees     
 *   - Version 1.2  (aout 1994: J. Vaucher) 
 *   - Version 1.0  (juin 1992)
 *        S. Some & J. Vaucher
 ********************************************************

STREAMS
    
  - int internet_connect( host, port) ==> fd:  Creates CLIENT socket connected 
                                           to <host_name, port#>; returns "fd"

  - int internet_server(port) ==> fd      Creates STREAM SERVER socket; 
                                           returns "fd"

  - int internet_accept (fd, &client_adr)  Receives connect request. 
                                  Returns "fd" of socket for private 
                                  interaction, As well as Client Address 

    - int  stream_write (fd, &Buffer, Len)
    - int  stream_read  (fd, &Buffer, Len)
    - char read_char     (fd)
    - int  read_string   (fd, &Buffer, Len, Delimiter_C)
    
DATAGRAMS 
            
    - int datagram_socket ()  => Creates Datagram socket and returns "fd"
    - int send_datagram   ( fd, &Buffer, Len, &SockAdr)
    - int receive_datagram( fd, &Buffer, Len, &SockAdr)
    
UTILITY
   
   - int sockadr_size  ():
   - int chars_avail (fd):   Returns the number of characters which can be read
                             (if ZERO, a read operation on "fd" will BLOCK)

ADDRESSING
 
    - host_aton ("www.jsp.umontrea.ca") ==> Host (integer)
    - char *host_ntoa( Host )           ==> "www.jsp.umontrea.ca"
    - char *host_ntod( Host )           ==> "132.102.54.12"
    
    - make_sockadr( &SockAdr, Host, Port)      
    - split_sockadr(&SockAdr, Host, Port)      
          
    - socketport  (int fd)        -> Port
    - serviceport ("http","tcp")  -> Port

  ============================================================= */


/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
          NETWORK ADDRESSING FUNCTIONS
   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  
   
These functions allow conversions between the various address formats 
used with INTERNET sockets.  For example:

Host addresses:
---------------
    Internally these are 32 bit unsigned integers, but externally, 2 text 
formats are be used:

   - Host address (alphabetic):     "www.iro.umontreal.ca" 
   - Host address in Dotted form:   "132.102.54.12"

Port addresses:
---------------
    Internally, these are 16 bit integers.  Most often port numbers for user 
sockets are allocated 'randomly' by the operating system.  On the other hand,
established operating system services such as FTP, MAIL or HTTP (Web servers) 
use ports with standard numbers which are the same on all machines connected to 
the internet.  Standard port numbers all have values below 1024 and are
listed in the file "/etc/services".

Socket addresses:
-----------------
    A socket address is a data structure which combines a HOST address and a
PORT address with a "tag" field which indicates the TYPE of the socket (UNIX or 
INTERNET). Socket addresses are required to set up a server's address (BIND) 
or to CONNECT to a stream server.  They are also used in operations 
which return the address of an incoming communication (ACCEPT, RECVFROM).

Note that nothing is a socket address indicates wether it IS used for
STREAM or DATAGRAM communication - that aspect is fixed by the SOCKET function
which allocates the required data structures.

Unfortunately for programmers, since socket addresses can have different sizes
and formats depending of their type, programs have to deal with various 
data structure definitions.  These 3 are typical:

   - struct sockaddr_in   : internet socket
   - struct sockaddr_un   : unix sockets
   - struct sockaddr      : generic socket type used in the prototype 
                            definitions of socket functions

Remember that functions which deal with sockets all require POINTERs to socket 
address - actual parameters of the form "&sa" rather than "sa".  Our functions, 
require pointers to 'internet' sockets (type: struct sockaddr_in * sockAdr ).
Internally, these are 'cast' into the type required by the UNIX procedures.


Socket numbers:
---------------
After a socket is created, it is referenced through a socket number which is
the same as a file descriptor (fd) for other UNIX IO (tty, file, dev, pipe,
etc...). These numbers are small integer values used by UNIX functions as
indexes into the local file table. Socket NUMBERS are thus quite different from
Socket ADDRESSES.


ROUTINES PROVIDED:
==================

a) Host addresses
-----------------
Conversion between the internal 32 bit version and the human-readable 
alphabetic formats.

- host_aton ( String ) -> integer
    
    The STRING can be IN either of the 2 standard formats:
"www.iro.umontreal.ca" or "132.102.54.12".  It can also be NULL or a zero
length string; in which case, the address of the machine on which the program
is running will be returned.
    
- host_ntoa ( Integer ) -> * char
 
   This returns a pointer to the STRING address in standard
("www.iro.umontreal.ca") format
   
- host_ntod ( Integer ) -> * char
 
   This returns a pointer to the address in dotted ("132.102.54.12") format


b) Port addresses
-----------------

- Port_aton( Service, Protocol) --> integer

    This gives the Port number for well-known services like "http" or "smtp"


c) Socket address <==> <Host,Port>
------------------------------------

    - make_socketAdr  ( ^sockaddr_in,  Host,  Port)
    - split_socketAdr ( ^sockaddr_in, ^Host, ^Port)

"make_socketAdr" initialises a socket address from Host and Port integer   
                 values.

"split_socketAdr" goes the other way; given an initialized socket address, it 
                  extracts and returns the Host and Port integer values.

    [Note: the character '^' is used to indicate that the parameter should be 
           passed by reference so that either i) a value can be returned 
           or ii) a data structure, used as input, doesn't have to be copied

d) Socket number ==> <Host,Port>
--------------------------------

- socket_to_HostPort(fd, ^Host, ^Port)

    Given a socket number (fd) this provides the Host and Port numbers as 
integer values.

aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa */   

        
#ifndef _INETSOCK_H_

#include <sys/socket.h>
#include <netinet/in.h>         /* pour struct sockaddr_in */
#include <arpa/inet.h>

#  define _INETSOCK_H_

   /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
   /*                       STREAMS                            */
   /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */


   /* ================  Creation of Streams ===================== */

   int internet_connect(char *host, int port );
   /* Used by CLIENTs to set up STREAM  connection to known server.
    * RETURNS: "fd".
    *
    *  Example:
    *     fd = internet_connect("www.iro.umontreal.ca", 80);
    *     read(fd,....);
    *     close(fd); */


   int internet_server(int Port);
   /* Used by SERVER to set up STREAM socket Port.  
    * A PORT number=0 means that the number will be assigned by the system and can
    *    be obtained from the returned "fd" with "sock2port".
    * RETURNS: "fd".
    *
    * Example:
    *    fd = internet_server(0);
    *    printf("Server receving on port: %d \n", socketport(fd));
    *    fd2 = inet_accept(fd,.....); */


   int internet_accept(int fd, struct sockaddr_in  *cli_adr);
   /* Used by SERVER to accept connection request.           
    * RETURNS: 1) new "fd" for private link to client
    *          2) the client address is returned in 2nd parameter
    *              
    * Example: 
    *    fd2 = inet_accept(fd, &cli_adr );
    *    split_sockadr( &cli_adr, C_host, & C_port);
    *    printf("Request from Host: %s, Port: %d\n", C_host,C_port);
    *    write( fd2, Msg, MsgLen);
    *    close(fd2); */



   /* ================  Input/Output to Streams ===================== */

   int stream_write(int fd, char *ptr, int  n);
   /* Write "n" bytes to a stream socket. */


   int stream_read(int fd, char * ptr, int nbytes);
   /* Read "n" bytes from a stream socket. */


   int read_char(int fd);
   /* Read a character. */

    
   int read_string(int fd, char *ptr, int maxlen, char C);
   /* Read a STRING delimited by character C.
    * Can be used to read a LINE with C = '\n'. */




   /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
   /*                       DATAGRAMS                          */
   /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */

   /* ================  Creation of Datagrams ===================== */

   int datagram_socket();
   /* Creates a Datagram Internet Socket and returns its "fd".  The PORT number
    * is assigned by the system and can be obtained from the returned "fd" with
    * "sock2port".
    * RETURNS: "fd".
    *
    * Example: 
    *    fd = udp_socket();         
    *    printf("Datagram socket created on PORT: %d \n", sock2port(fd)); */



   /* ================  Input/Output to Streams ===================== */

   int send_datagram(int fd, char *buff, int n, struct sockaddr_in *DestAdr);
   /* Envoie buff. */


   int receive_datagram(int fd, char *buff, int n, struct sockaddr_in *PeerAdr);
   /* Recois buff. */



   /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
   /*            NETWORK ADDRESSING FUNCTIONS                  */
   /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */

   /* Definition pour host_name. */
   #define PATHSIZE  100
  
   char *host_name();
   /* Return the host name. */


   int32_t host_aton(const char *name);
   /* Takes HOST names in either the numeric 132.104.25.16 or the usual format
    * "www.iro.umontreal.ca" and returns a 32 bit address. */


   char *host_ntoa(u_long Host);
   /* Produces the "nil.iro.umontreal.ca". */


   char *host_ntod(u_long Host);
   /*  Produces the "132.45.44.01" form of an INET address. */


   void make_sockadr(struct sockaddr_in *SockAdr, u_long Host, int Port);
   /* Initializes a Socket Address from integer Host and Port addresses. */


   void split_sockadr(struct sockaddr_in *SockAdr, u_long  *Host, int *Port);
   /* Inverse of "make_inetadr": extracts the Host and Port numbers from a Socket
    * Address.  Can be used to get information about CLIENTS for both STREAM and
    * DATAGRAM connections.
    *     
    * Example: 
    *    fd2 = internet_accept(fd, & cli_adr );
    *    split_sockadr( cli_adr, &C_host, &C_port);
    *    printf("Request from Host: %s, Port: %d\n", Host_ntoa(C_host), C_port);
    *    write( fd2, Msg, MsgLen); */


   int socketport(int fd);
   /* Retourne le numero de port auquel est lie le socket "fd". */


   int serviceport(char *service, char *protocol);
   /* Looks at /etc/srvices to find PORT number for a given service.
    * i.e. service2port("http","tcp")  returns 80.
    *   
    * Note: protocol = { "tcp", "udp" }. */


   int sockadr_size();
   /* Allows Simula programs to create buffers (TEXT) of correct size for
    * ADDRESSES as required by functions like INET_ACCEPT. */

#endif

/*****************************/
/* Fin du fichier inetsock.h */
/*****************************/
