/****************************-*-C-*-***********************************
 * $Id: params.h,v 2.5 2001/10/01 08:12:23 anarcat Exp $
 **********************************************************************
 * Macros de configurations et autres
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * Voir le fichier ChangeLog pour l'historique du fichier
 *********************************************************************/

#ifndef _PARAMS_H
#define _PARAMS_H

#define SERVERNAME "MicroHttpd/2.2"

#ifndef BUFSIZ
#define BUFSIZ 8192
#endif

#include <sys/param.h>

#ifndef MAXPATHLEN
#define MAXPATHLEN 1024
#warning "Defining MAXPATHLEN 1024"
#endif

#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 255
#endif

#ifndef FD_COPY
#define	FD_COPY(f, t)	bcopy(f, t, sizeof(*(f)))
#endif

/**
 * Codes de m�thodes HTTP/1.1
 **/
#define HTTP_ERROR              0
#define HTTP_UNKNOWN            1
#define HTTP_OPTIONS            2     /* Non implant� */
#define HTTP_GET                4
#define HTTP_HEAD               8
#define HTTP_POST               16    /* Non implant� */
#define HTTP_PUT                32    /* idem */
#define HTTP_DELETE             64    /* idem */
#define HTTP_CONNECT            128   /* idem */

/**
 * Versions HTTP
 *
 * HTTP/0.9: non-d�fini officielement, seulement GET d'implant�
 * HTTP/1.0: RFC 1945
 * HTTP/1.1: RFC 2616
 **/
#define HTTP_ERROR_VER          256
#define HTTP_UNKNOWN_VER        512
#define HTTP_0_9                1024
#define HTTP_1_0                2048
#define HTTP_1_1                4096

#endif
