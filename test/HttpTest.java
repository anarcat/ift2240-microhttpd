import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

class HttpTest {

    static void sep()
    {
        System.out.println("======================================================================");
    }

    public static void main(String args[]) {

        HttpRequest test = null;

        String host = "localhost";
        int port = 50080;
        String resource = "/";

        sep();
        System.out.println("Testing HttpRequest on url: http://" +
                           host + ":" + port + resource);
        
        try {
            test = new HttpRequest(host, port);
        } catch (UnknownHostException exc) {
            System.out.println("Unknown host while creating HttpRequest: " +
                               exc.getMessage());
            System.exit(1);
        } catch (IOException exc) {
            System.out.println("IO Error while creating HttpRequest: " +
                               exc.getMessage());
            System.exit(2);
        } 
        
        System.out.println("Created HttpRequest");
        System.out.println("Sending get request: "
                           + resource);        
        try {
            System.out.println(test.get(resource, true));
        } catch (IOException exc) {
            System.out.println("Error in sending GET request: "
                               + exc.getMessage());
            System.exit(3);
        }

        try {
            test.close();
        } catch (IOException exc) {
            System.out.println("IO Error in trying to close HttpRequest: "
                               + exc.getMessage());
            System.exit(5);
        }
        System.out.println("Closed HttpRequest, all done");
        sep();
    }

}
