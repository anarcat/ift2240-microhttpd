#include <stdio.h>
#include <stdlib.h>
#include "../misc.h"

int main(int argc, char** argv) {

  (void)printf
    ("======================================================================\n\
Test procedures for dirname()\n\n\
Trying dirname(%s): %s\nDone!\n\n\
======================================================================\n",
     argv[1], dirname(argv[1]));
  return 0;
  
}
