#include <stdio.h>
#include <unistd.h>

#include "../writer.h"
#include "../misc.h"
#include "../serveur.h"

char *ip;
int clientVersion;

int main (int argc, char **argv) {

  char *path;
  ip = "127.0.0.1";
  
  printf("\
======================================================================\n\
writeCGI() test utility\n\n\
Script output should normally appear to desc 2.\n\
calling writeCGI(2, \"%s\"):\n", argv[1]);

  writeCGI(2, argv[1], argv[1], "a=1");
  printf("\nall done.\n\
======================================================================\n");
  return 0;
}
