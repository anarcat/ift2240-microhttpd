#include <unistd.h>
#include <stdio.h>
#include "../misc.h"

#include "../params.h"

int main (int argc, char **argv) {

  char path[MAXPATHLEN];
  struct stat fstats;
  int code;
  
  if (chdir ("/home/spidey/HTML") < 0)
    printf("chdir failed\n");
  else
    printf("chdir to /home/spidey/HTML\n");
  
  printf
    ("======================================================================\n\
findResource() test utiliy\n\
testing: findResource(\"%s\"):\n", argv[1]);
  code = findResource(argv[1],
                      path, MAXPATHLEN,
                      &fstats);
  printf("path %s, result code: %d\n",
         path, code);
  printf("\nAll done.\n\
======================================================================\n");
  return 0;
  
}
