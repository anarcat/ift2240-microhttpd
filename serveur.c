/****************************-*-C-*-***********************************
 * $Id: serveur.c,v 2.14 2001/10/01 08:45:19 anarcat Exp $
 **********************************************************************
 * Serveur HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include <err.h>
#include <errno.h>

#include <sys/types.h>

#include <sys/socket.h>         /* idem */
#include <netinet/in.h>         /* pour struct sockaddr_in */
#include <arpa/inet.h>

#include <string.h>             /* strcmp et strtok */
#include <unistd.h>
#include <stdlib.h>

#include <sys/wait.h>
#include <sys/time.h>

#include <signal.h>

#include "params.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "serveur.h"

#include "reader.h"
#include "writer.h"
#include "misc.h"

#ifdef HAVE_INETSOCK_H
#include <inetsock.h>
#endif

int serverfd;                   /* le descripteur du socket du serveur */
int clientVersion = HTTP_1_1;   /* la version courante HTTP */
char* ip;                       /* l'addresse du client */

/**
 * Fonction de nettoyage pour r�pondre � un signal ou autre.
 **/
void cleanup() {
  close(serverfd);
#ifdef HOSTFILE
  unlink(HOSTFILE);
#endif
}

/**
 * Proc�dure d'arr�t du serveur
 *
 * Si e est diff�rent de 0, err() est utilis� pour rapporter l'erreur.
 **/
void stop(int e) {
  int status, pid = -1, pid2 = -1;
  cleanup();
  warnx("waiting for childs to terminate");
  while ((pid = waitpid(-1, &status, WUNTRACED)) >= 0) {
    if (WIFSTOPPED(status)) {
      if (pid != pid2) {
        warnx("sending SIGINT to stopped child pid %d", pid);
        kill(pid, SIGINT);
      } else {
        warnx("sending SIGKILL to stopped child pid %d", pid);
        kill(pid, SIGKILL);
      }
    }
    pid2 = pid;
  }
  if (errno == ECHILD)
    warn("done waiting");
  else
    warn("error waiting");
  if (e == 0)
    errx(0,"stopped server");
  else
    errx(0, "stopped server, died with signal %d", e);
}

/***********************************************************************
 * Initialisation du serveur sur le port donn�
 *
 * Cr�� un socket TCP, "bind" le serveur sur le port donn�, et "listen"
 * sur le port.
 *
 * Il est possible que cette fonction ne retourne pas car elle appelle
 * err() si une erreur majeure survient.
 **********************************************************************/
void setup_server(int *port) {

  struct sockaddr_in serveraddr; /* composantes de l'adresse du serveur */
  char hostname[MAXHOSTNAMELEN];
  
  /* trace d'activit� � toutes les 120 secondes (2 min) */
  struct itimerval value;

  warnx("setting umask to 022");
  umask(022);                   /* umask 022, write only by user */
  
#ifdef USE_HOME
  if (chdir(getenv("HOME")) < 0) 
    warn("can't chdir(2) to $HOME dir (using current directory)");
  else
#endif
#ifdef HT_DIR
    if (chdir(HT_DIR) < 0) {
#ifdef USE_HOME
      warn("can't chdir(2) to $HOME/%s dir (using $HOME)", HT_DIR);
#else
      warn("can't chdir(2) to %s dir (using current dir)", HT_DIR);
#endif
    }
#else
  ;
#endif

  signal( SIGALRM, alive );     /* alive() sur ALRM */
  signal( SIGINT, stop );        /* cleanup avant interrupt */
  signal( SIGHUP, stop );       /* idem */
  
  atexit(cleanup);              /* � la sortie, on appelle cleanup() */

  timerclear(&value.it_value);
  timerclear(&value.it_interval);
  
  value.it_value.tv_sec = 120;
  value.it_value.tv_usec = 0;
  
  value.it_interval.tv_sec = 120; /* � toutes les 120 s */
  value.it_interval.tv_usec = 0;
  
  if (setitimer(ITIMER_REAL, &value, NULL) < 0)
    warn("can't set timer");

  /**
   * Cr�ation du socket
   **/
  serverfd = socket(AF_INET, SOCK_STREAM, 0); /* Cr�e le socket */

  if (serverfd < 0)               /* N'a pas pu �tre cr�� */
    err(-1, "main socket creation failed"); /* on signale une erreur */

  /**
   * Bind the socket to an address
   *
   * Ici, on le bind � n'importe adresse dans la famille 'INET'
   * (internet) sur le port donn�
   **/
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl( INADDR_ANY );
  serveraddr.sin_port = htons( *port );
   
  if( bind( serverfd, (struct sockaddr*) &serveraddr,
            sizeof( struct sockaddr_in) ) < 0 )
    err(-1, "can't bind to inet socket on port %d", *port);
  
  /**
   * On �coute sur le socket
   **/
  if (listen (serverfd, PENDING_CON) < 0)
    err(-1, "can't listen to socket");

  /**
   * On identifie le hostname et le port utilis�
   **/
  gethostname(hostname, MAXHOSTNAMELEN);

#ifdef HAVE_INETSOCK_H
  *port = socketport(serverfd);
#else
  *port = ntohs(serveraddr.sin_port);
#endif
  
  warnx("%s: started on host %s port %d, waiting for connection.\n",
        SERVERNAME, hostname, *port);

#ifdef HOSTFILE
  writeHostfile(hostname, *port);
#endif
  
} /* fin setup_server() */

/***********************************************************************
 * main
 **********************************************************************/
int main(int argc, char** argv) {

  int clientfd;                 /* le descripteurs de connections client */
  int port = DEFAULT_PORT;      /* le port o� le serveur �coute */
  struct sockaddr_in clientaddr; /* adresse client */
  unsigned int clientaddr_len;           /* taille */
#ifdef STOP
  int inCnt = 0;                /* compteurs d'octets sur stdin */
  char buffer[BUFSIZ];
#endif
#if 0
  struct timeval timeout;
#endif
#ifdef USE_FORK
  int pid;
#endif
  fd_set all_set, rset, wset, eset;

  /*********************************************************************
   * On initialise le num�ro de port � utiliser.
   *
   * Le port et seulement le port est cens� �tre pass� en argument.
   ********************************************************************/
  if (argc < 2) {               /* nombre d'arguments incorrect */
    warnx("no port specified, trying %d.", DEFAULT_PORT);
    port = DEFAULT_PORT;        /* on met le port par d�faut */
  } else {                      /* correct number of args */
    char *where;
    port = strtol(argv[1], &where, 10);
    if (!where || port < 0 || port > 65535) { /* on v�rifie le range */
      warnx("%s: bad port number, using default", argv[1]);
      port = DEFAULT_PORT;
    }
  }

  setup_server(&port);

  FD_ZERO(&all_set);
  FD_SET(0, &all_set);
  FD_SET(serverfd, &all_set);
  
  FD_COPY(&all_set, &rset);

  /**
   * On prends une seule connection � la fois
   *
   * On pourrait �galement faire un fork() pour pouvoir recevoir
   * plusieurs requ�tes en parall�le
   **/
  while (1) {

    switch (select(serverfd+1, &rset, NULL, NULL, NULL)) {
      
    case -1:
      if (errno == EINTR)       /* interrupted */
        continue;               /* continue */
      else {                     /* normal exit */
        warn("error in select");
        break;                  /* break and exit */
      }
      break;
    case 0:                     /* aucun desc disponible */
      /* looking for straw childs to kill */
      {
        pid_t pid;
        int status;
        pid = waitpid(-1, &status, WNOHANG | WUNTRACED);

        switch (pid) {
        case -1:                          /* error */
          if (errno != ECHILD)
            warn("error waiting for childs in main loop");
          break;
        case 0:                           /* no child */
          break;
        default:                          /* found darling */
          if (WIFSTOPPED(status)) {
            switch (WSTOPSIG(status)) {
            case SIGTTIN:
            case SIGTTOU:
              warnx("sending SIGINT to bkgnd proc wating for I/O on terminal");
              kill(pid, SIGINT);
              break;
            case SIGTSTP:
            case SIGSTOP:
              break;
            }
          } else if (WIFEXITED(status))
            warnx("child exited with staus %d", WEXITSTATUS(status));
          else if (WIFSIGNALED(status)) {
            if (WCOREDUMP(status))
              warnx("child died with signal %d, core dump", WTERMSIG(status));
            else
              warnx("child died with signal %d", WTERMSIG(status));
          }
          break;
        }
      } /* killing childs */
      break;
      
    default:                    /* descripteurs disponibles */
      
      if (FD_ISSET(serverfd, &rset)) { /* entr�e sur socket */
        
        if ( (clientfd = accept (serverfd,(struct sockaddr*) &clientaddr, &clientaddr_len)) > 0) {
          
          ip = inet_ntoa(clientaddr.sin_addr);
          
#ifdef USE_FORK                           /* section parall�le */
          pid = fork();   /* child creation  */
          if (pid == 0) { /* child */
#endif
            warnx("connection from client %s", ip);
            readRequest(clientfd);       /* traitement de la requ�te */
            close (clientfd);            /* d�connection du client */
            warnx("client %s disconnected", ip);
            
#ifdef USE_FORK                          /* section parall�le */
            _exit(0);                    /* sortie du child */
          } else if (pid < 0)            /* parent - ERROR */
            warn("error in forking for new connection");
          else warnx("forked new child for connection");
          close(clientfd);               /* on quitte le client chez le parent */
#endif
        } else /* Si on est sorti de accept � cause d'une erreur */
          warn("can't get connections from socket");

      }
#ifdef STOP
      else if (FD_ISSET(0, &rset)) {     /* entr�e sur stdin */
        
        if (read(0, &buffer[inCnt], 1) > 0) {
          if ((inCnt >= BUFSIZ - 2) ||
              (buffer[inCnt] == '\n')) {
            buffer[inCnt] = '\0';
            if (!strncmp(buffer, "STOP", 4)) {
              warnx("stopping server at user request on terminal");
              stop(0);
            } else if (inCnt > 1)
              warnx("%s: unknown command", buffer);

            inCnt = 0;
          } else inCnt++;
        } else warn("error reading on stdin");
        
      } /* fin analyse stdin */
#endif
      
      FD_COPY(&all_set, &rset);
      FD_COPY(&all_set, &eset);
      FD_COPY(&all_set, &wset);
      break;                    /* fin analyse des descripteurs */
      
    } /* switch (select()) */
  } /* boucle sur select() */
  
  return 0;
  
}
