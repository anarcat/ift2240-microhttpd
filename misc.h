/****************************-*-C-*-***********************************
 * $Id: misc.h,v 2.5 2000-04-12 18:45:02-04 spidey Exp spidey $
 **********************************************************************
 * Quelques fonctions utiles
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef _MISC_H
#define _MISC_H

#include <sys/stat.h>

void alive(int n);

/**********************************************************************
 * Trouve le path de la resource demand�e
 *
 * Retourne le response code HTTP pour l'url demand�. Si l'url contient
 * ".." (EPERM), le code est 403, s'il est trop long (ENAMETOOLONG), le
 * code est 404. Sinon, le code d�pend de l'appel de statFile() sur le
 * fichier.
 *
 * Si l'url se traduit en un r�pertoire, le fichier INDEX_FILE est
 * cherch� dans le r�pertoire (avec statFile()). Si le fichier est
 * disponible (code 200), le stat et le path retourn� sera celui du
 * fichier d'index.
 *
 * Pourra �tre utilis� dans le futur pour implanter des r�pertoires
 * "virtuels".
 *********************************************************************/
int findResource(char* url,     /* l'url � traduire */
                 char *path, int path_len, /* path et taille max */
                 struct stat *fstats); /* o� stocker le stat */

/***********************************************************************
 * Donne le response code d'un HEAD sur un fichier
 *
 * Si NULL est donn� comme path, on analyse errno pour d�terminer un
 * responseCode appropri�.
 *
 * Sinon, on fait un stat sur le path. Si il y a erreur, elle est
 * analys�e (errno). Des erreurs IO (EFAULT, EIO) retournent un code 500
 * (Server error). Les erreurs de permissions retourne 403 (Forbidden).
 * Les erreurs dans le path (ENOTDIR, ENAMETOOLONG, ENOENT, ELOOP)
 * retournent 404 (Not Found).
 * 
 * Les r�pertoires et les autres fichiers n'ayant pas S_IFREG comme mode
 * sont 403 (Forbidden).
 *
 * Retourne le code HTTP (200, 400, etc) ou -1 si une erreur s'est
 * produite dans writeHttpHeader().
 *
 * Le struct stat pass� en param�tre sera initialis� avec un appel
 * de stat(2) sur le path.
 **********************************************************************/
int statFile(char *path, struct stat *fstats);

/***********************************************************************
 * Cherche un "query string" (...?<str>) dans l'url donn�.
 *
 * Retourne un pointeur vers la position du d�but de la cha�ne et modifie
 * la cha�ne donn� pour qu'elle se termine au lieu du '?', ou NULL si
 * aucun ? est trouv�
 ***********************************************************************/
char* query_string(char* url);

/***********************************************************************
 * Retourne la partie "r�pertoire" du fichier donn�
 *
 * Bas� sur dirname(1) sur FreeBSD (http://www.freebsd.org/):
 * @(#)dirname.c	8.4 (Berkeley) 5/4/95
 **********************************************************************/
char* dirname(char *path);

/***********************************************************************
 * Retire la partie "fichier" d'un path
 *
 * Donne le nom du fichier sans le path pour y acc�der.
 * Bas� sur basename(1) de FreeBSD (http://www.freebsd.org):
 * @(#)basename.c	8.4 (Berkeley) 5/4/95
 **********************************************************************/
char* basename(char *path);

/**
 * la liste des 'symboles' correspondants � macros (ex.:
 * C_STRING[C_HTML] == "html" et C_STRING[C_HTML & FAMILY] == "text")
 **/
extern const char *C_STRING[];  /* d�fini dans misc.c */

/***********************************************************************
 * Determine le 'content-type' du fichier point� par path et d�crit par
 * fstats
 **********************************************************************/
int content_type(char *path, struct stat *fstats);

/**
 * D�finition des extensions
 *
 * De ftp://ftp.isi.edu/in-notes/iana/assignments/media-types/media-types
 **/

/* famille text */
#define C_TEXT          0x00 /* 0000 0000 */

#define C_PLAIN         0x01 /* [RFC2646,RFC2046] */
#define C_RICHTEXT      0x02 /* [RFC2045,RFC2046] */
#define C_ENRICHED      0x03 /* [RFC1896] */
#define C_HTML          0x04 /* [RFC1866] */
#define C_SGML          0x05 /* [RFC1874] */
#define C_RFC1822       0x06 /* [RFC1892] */
#define C_CSS           0x07 /* [RFC2318] */
#define C_XML           0x08 /* [RFC2376] */
#define C_RTF           0x09 /* [Lindner] */
#define C_DIRECTORY     0x0A /* [RFC2425] */
#define C_CALENDAR      0x0B /* [RFC2445] */
                                   
/* famille multipart */
#define C_MULTIPART     0x10

#define C_MIXED         0x11 /* [RFC2045,RFC2046] */
#define C_ALTERNATIVE   0x12 /* [RFC2045,RFC2046] */
#define C_DIGEST        0x13 /* [RFC2045,RFC2046 ]*/
#define C_PARALLEL      0x14 /* [RFC2045,RFC2046 ]*/
#define C_APPLEDOUBLE   0x15 /* [MacMime,Patrik Faltstrom ]*/
#define C_HEADER_SET    0x16 /* [Dave Crocker ]*/
#define C_FORM_DATA     0x17 /* [RFC2388 ]*/
#define C_RELATED	0x18 /* [RFC2387 ]*/
#define C_REPORT        0x19 /* [RFC1892 ]*/
#define C_VOICE_MESSAGE 0x1A /* [RFC2421,RFC2423 ]*/
#define C_SIGNED        0x1B /* [RFC1847 ]*/
#define C_ENCRYPTED     0x1C /* [RFC1847 ]*/
#define C_BYTERANGES    0x1D /* [RFC2068 ]*/

/* famille message */
#define C_MESSAGE       0x20

#define C_RFC822        0x21 /* [RFC2045,RFC2046] */
#define C_PARTIAL       0x22 /* [RFC2045,RFC2046] */
#define C_EXTERNAL_BODY 0x23 /* [RFC2045,RFC2046] */
#define C_NEWS          0x24 /* [RFC 1036, Henry Spencer] */
#define C_HTTP          0x25 /* [RFC2616] */
#define C_DELIVERY_STATUS 0x26 /* [RFC1894] */
#define C_DISPOSITION_NOTIFICATION 0x27 /* [RFC2298] */
#define C_S_HTTP        0x28 /* [RFC2660] */

/* famille application */
#define C_APPLICATION   0x30

/**
 * On ne peut pas esp�rer repr�senter tous les 231 types de la famille.
 * 
 * On enl�ve donc les plus rarissimes ou doubl�s ou inutiles, avec une
 * nette pr�f�rence � ceux d�finis par RFC
 **/
#define C_OCTET_STREAM  0x31 /* [RFC2045,RFC2046] */
#define C_POSTSCRIPT    0x32 /* [RFC2045,RFC2046] */
#define C_ODA           0x33 /* [RFC2045,RFC2046] */
#define C_NEWS_MESSAGE_ID 0x34 /* [RFC1036, Henry Spencer] */
#define C_NEWS_TRANSMISSION 0x35 /* [RFC1036, Henry Spencer] */
#define C_PDF           0x36 /* [Paul Lindner] */
#define C_ZIP           0x37 /* [Paul Lindner] */
#define C_MSWORD        0x38 /* [Paul Lindner] */
#define C_MATHEMATICA   0x39 /* [Van Nostern] */
#define C_PGP_ENCRYPTED 0x3A /* [RFC2015] */
#define C_PGP_SIGNATURE 0x3B /* [RFC2015] */
#define C_PGP_KEYS      0x3C /* [RFC2015] */
#define C_PKCS7_MIME    0x3D /* [RFC2311] */
#define C_PKCS7_SIGNATURE 0x3E /* [RFC2311] */
#define C_PKCS10        0x3F /* [RFC2311] */

/* famille image */
#define C_IMAGE         0x40 /* 0010 0000 */

#define C_JPEG          0x41 /* [RFC2045,RFC2046] */
#define C_GIF           0x42 /* [RFC2045,RFC2046] */
#define C_IEF           0x43 /* Image Exchange Format [RFC1314] */
#define C_G3FAX         0x44 /* [RFC1494] */
#define C_TIFF          0x45 /* Tag Image File Format [RFC2302] */
#define C_CGM		0x46 /* Computer Graphics Metafile [Francis] */
#define C_NAPLPS        0x47 /* [Ferber] */
#define C_PNG           0x48 /* [Randers-Pehrson] */

/* famille audio */
#define C_AUDIO         0x50

#define C_BASIC         0x51 /* [RFC2045,RFC2046] */
#define C_32KADPCM      0x52 /* [RFC2421,RFC2422] */
#define C_L16           0x53 /* [RFC2586] */

/* famille video */
#define C_VIDEO         0x60

#define C_MPEG          0x61 /* [RFC2045,RFC2046] */
#define C_QUICKTIME     0x62 /* [Paul Lindner] */

/* famille model */
#define C_MODEL         0x70 /* [RFC2077] */

#define C_IGES          0x71 /* [Parks] */
#define C_VRML          0x72 /* [RFC2077] */
#define C_MESH          0x73                               /* [RFC2077] */

/* type sp�cial utilis� � l'interne */
#define C_CGI           0x7F /* 1111 1111 */

/* masks */
#define C_FAMILY        0xF0 /* 1111 0000 */
#define C_TYPE          0x0F /* 0000 1111 */

#endif
