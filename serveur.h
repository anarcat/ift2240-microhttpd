/****************************-*-C-*-***********************************
 * $Id: serveur.h,v 2.2 2000/04/12 22:48:23 spidey Exp $
 **********************************************************************
 * Serveur HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef _SERVEUR_H
#define _SERVEUR_H

#include "params.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

extern int serverfd;        /* le descripteur du socket du serveur */
extern int clientVersion;   /* la version courante HTTP */
extern char* ip;            /* l'addresse du client */

/**
 * Fonction de nettoyage pour r�pondre � un signal ou autre.
 **/
void cleanup();

#ifdef STOP
/**
 * Proc�dure d'arr�t du serveur
 *
 * Si e est diff�rent de 0, err() est utilis� pour rapporter l'erreur.
 **/
void stop(int e);
#endif

/***********************************************************************
 * Initialisation du serveur sur le port donn�
 *
 * Cr�� un socket TCP, "bind" le serveur sur le port donn�, et "listen"
 * sur le port.
 *
 * Il est possible que cette fonction ne retourne pas car elle appelle
 * err() si une erreur majeure survient.
 **********************************************************************/
void setup_server(int *port); /* fin setup_server() */

/***********************************************************************
 * main
 **********************************************************************/
int main(int argc, char** argv);

#endif
