/****************************-*-C-*-***********************************
 * $Id: parser.c,v 2.2 2000/04/12 22:28:44 spidey Exp $
 **********************************************************************
 * Parser HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include <string.h>             /* strcmp */
#include <stdlib.h>             /* strtol */

#include <ctype.h>              /* isdigit() */

#include "parser.h"

#include "params.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "serveur.h"

/***********************************************************************
 * Parser
 *
 * RFC 2616 dit que:
 *
 * Request-Line = Method SP Request-URI SP HTTP-Version CRLF
 **********************************************************************/

/***********************************************************************
 * Retourne la m�thode HTTP d'une cha�ne
 *
 * Les m�thodes HTTP/1.1 sont, d'apr�s RFC 2616:
 * 
 * Method  = "OPTIONS" | "GET" | "HEAD" | "POST" | "PUT" | "DELETE" |
 *           "TRACE" | "CONNECT"
 *
 * Il y a �galement la m�thode sp�ciale et optionelle "STOP"
 * 
 * Retourne une macro d�signant la m�thode. Retourne HTTP_UNKNOWN si 
 * la m�thode est inconnue pour la version HTTP courante ou HTTP_ERROR
 * si buffer est NULL. 
 ***********************************************************************/
int parseMethod(char* buffer) {

  int code = HTTP_UNKNOWN;

  if (buffer) {
    /*
     * Un switch sur la premi�re lettre nous permet d'�viter plusieurs
     * comparaisons inutiles
     */
    switch(buffer[0]) {
    case 'O':
      if (clientVersion >= HTTP_1_1 &&
          !strcmp(buffer, "OPTIONS"))
        code = HTTP_OPTIONS;
      break;
    case 'G':
      if (!strcmp(buffer, "GET"))
        code = HTTP_GET;
      break;
    case 'H':
      if (clientVersion >= HTTP_1_0 &&
          !strcmp(buffer, "HEAD"))
        code = HTTP_HEAD;
      break;
    case 'P':
      if (clientVersion >= HTTP_1_0 &&
          !strcmp(buffer, "POST"))
        code = HTTP_POST;
      else if (clientVersion >= HTTP_1_1 &&
               !strcmp(buffer, "PUT"))
        code = HTTP_PUT;
      break;
    case 'D':
      if (clientVersion >= HTTP_1_1 &&
          !strcmp(buffer, "DELETE"))
        code = HTTP_DELETE;
      break;
    case 'C':
      if (clientVersion >= HTTP_1_1 &&
          !strcmp(buffer, "CONNECT"))
        code = HTTP_CONNECT;
      break;
    default:
      code = HTTP_UNKNOWN;
      break;
    }
  } else code = HTTP_ERROR;        /* on n'a pas trouv� de token */

  return code;
  
} /* fin parseMethod() */

/************************************************************************
 * Retourne le fichier sans le slash ou NULL s'il n'y a pas de slash au
 * d�but de la cha�ne.
 ***********************************************************************/
char* parseUrl(char* buffer) {

  if (buffer) {
    if (buffer[0] == '/') buffer++;   /* on saute le premier slash */
    else buffer = NULL;             /* erreur, pas de slash */
  }
  return buffer;
  
} /* fin parseUrl() */

/************************************************************************
 * Interpr�te la cha�ne donn�e comme une version HTTP
 *
 * D'apr�s RFC 2616: HTTP-Version = "HTTP" "/" 1*DIGIT "." 1*DIGIT
 *
 * Retourne HTTP_0_9 si buffer est null ou si buffer = "HTTP/0.9" ou "".
 * Retourne HTTP_1_0 si buffer est "HTTP/1.0", HTTP_1_1 pour "HTTP/1.1"
 * et HTTP_UNKNOWN_VER dans les autres cas valides et HTTP_ERROR_VER
 * dans les autres cas.
 ***********************************************************************/
int parseVersion(char* buffer) {

  char* where;                  /* where number parsing stopped */
  int version = HTTP_ERROR_VER;

  int major, minor;             /* version "majeure" et "mineure" */

  if (buffer && (strlen(buffer) > 0)) {
    if (!strncmp(buffer, "HTTP/", 5)) { /* d�but HTTP/ correct */
      if (isdigit(buffer[5])) {
        major = strtol(&buffer[5], &where, 10);
        if (where) {
          minor = strtol(where+1, &where, 10);
          switch (major) {
          case 0:
            if (minor == 9)
              version = HTTP_0_9;
            else
              version = HTTP_UNKNOWN_VER;
            break;
          case 1:
            switch (minor) {
            case 0:
              version = HTTP_1_0;
              break;
            case 1:
              version = HTTP_1_1;
              break;
            default:
              version = HTTP_UNKNOWN_VER;
            }
            break;
          default:
            version = HTTP_UNKNOWN_VER;
            break;
          }
        }
      }
    }
  } else version = HTTP_0_9;

  return version;
  
} /* fin parseVersion */

/***********************************************************************
 * Ceci cherche un espace ou une fin de ligne
 *
 * Retourne le token termin� par une espace et �dite le pointeur donn�
 * pour qu'il pointe vers la suite de la cha�ne
 **********************************************************************/
char* nextToken(char** buffer) {

  char *buf = *buffer;
  char *ret;
  
  /* on cherche un espace ou une fin de ligne */
  while (*buf && !isspace(*buf) &&
         *buf != '\r' && *buf != '\n' )
    buf++;

  if (buf) {
    *buf = '\0';                /* on termine la cha�ne ici */
    buf++;
  } else
    *buf = '\0';
  
  while (*buf && isspace(*buf))  /* on saute les espaces */
    buf++;

  ret = *buffer;
  *buffer = buf;
  return ret;
  
} /* fin nextMethodTok */
