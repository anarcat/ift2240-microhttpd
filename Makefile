SHELL:=/bin/sh

PROGS:=client serveur # les programmes � construire

SRCS=${SSRCS} ${CSRCS} # les fichiers source
OBJ=${SOBJ} ${COBJS} # les fichiers 'objet' � cleaner

# fichiers du serveur
SSRCS:=serveur.c method.c parser.c writer.c reader.c misc.c
SOBJ=${SSRCS:%.c=%.o}

# fichiers du client
CSRCS:=Client.java HttpRequest.java ServerList.java
COBJS:=${CSRCS:%.java=%.class}

CFLAGS=-Wall -DHAVE_CONFIG_H # on inclus les .h du r�pertoire de contrib

JAVAC:=/usr/local/java/bin/javac
JAVA:=/usr/local/java/bin/java

TESTDIR:=test

all: ${PROGS}		# all construit les programmes

version:		# imprime de l'information sur le projet
	more A_LIRE

exec_client: client	# teste le client
	${JAVA} Client

exec_serveur: serveur	# teste le serveur
	./serveur 0

clean:			# nettoyage
	\rm -f ${PROG} ${OBJ} *.class a.out *.core

tests:			# tests de certaines fonctions particuli�res
	cd ${TESTDIR} && gmake

client: Client.class	# client d�pend de la classe

%.class: %.java
#	/usr/local/lib/inclure/inclure.exec sh jdk-1.1.5
	${JAVAC} $<

Client.class: HttpRequest.class ServerList.class
	${JAVAC} $<

serveur: ${SOBJ}
	${CC} ${CFLAGS} ${SOBJ} -o serveur

serveur.o: reader.o writer.o misc.o params.h serveur.h config.h

method.o: writer.o misc.o method.h params.h config.h

writer.o: misc.o writer.h params.h serveur.h config.h

reader.o: writer.o parser.o method.o misc.o reader.h params.h serveur.h config.h

parser.o: parser.h params.h serveur.h config.h

misc.o: misc.h params.h config.h