/****************************-*-C-*-***********************************
 * $Id: misc.c,v 2.8 2001/10/01 05:52:24 anarcat Exp $
 **********************************************************************
 * Quelques fonctions utiles
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include <errno.h>
#include <err.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "misc.h"

#include "params.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_UTILS_H
#include <utils.h>
#endif

/**
 * Prouve que le serveur est en vie
 **/
void alive(int n) {
  warnx("Timeout expired (received signal %d), pid: %d", n, getpid());
}

/**********************************************************************
 * Trouve le path de la resource demand�e
 *
 * Retourne le response code HTTP pour l'url demand�. Si l'url contient
 * ".." (EPERM), le code est 403, s'il est trop long (ENAMETOOLONG), le
 * code est 404. Sinon, le code d�pend de l'appel de statFile() sur le
 * fichier.
 *
 * Si l'url se traduit en un r�pertoire, le fichier INDEX_FILE est
 * cherch� dans le r�pertoire (avec statFile()). Si le fichier est
 * disponible (code 200), le stat et le path retourn� sera celui du
 * fichier d'index.
 *
 * Si path ou fstats est NULL, -1 est retourn� et errno == EFAULT.
 *
 * Pourra �tre utilis� dans le futur pour implanter des r�pertoires
 * "virtuels".
 *
 * Une autre id�e serait peut-�tre de "cacher" ces r�sultats pour un
 * acc�s plus rapide.
 *********************************************************************/
int findResource(char* url,     /* l'url � traduire */
                 char *path, int path_len, /* path et taille max */
                 struct stat *fstats) { /* o� stocker le stat */
  
  char *urlBak = url;
  int len, code = 0;

  if ( (fstats == NULL) || (path == NULL)) {
    errno = EFAULT;
    return -1;
  }
  
  {  /* first clear the path */
    int i; for (i = 0; i < path_len; i++)
    path[i] = '\0';
  }
  
  /* cr�ation du path */
  /* cas url == NULL, len == 0 et 1 */
  if (!url || (len = strlen(url)) == 0 ||/* no url given -> base dir */
      (len == 1 && url[0] == '.')) /* url = "." */
    strncat(path, "./", path_len); /* path = "./" */
  
  else {                    /* len > 1 */
    
    /* on v�rifie la taille de l'url */
    if ( len > path_len-strlen(path)) {/* trop long */
      errno = ENAMETOOLONG;
      code = 404;
      path = NULL;
    } else {               /* taille ok */
      
#ifdef USE_UTILS_H
      if (IsFileDangerous(url)) {
        errno = EPERM;
        code = 403;
        path = NULL;
      }
#else
      int i = 0;
      /* on v�rifie que le path ne comprenne pas de ".." */  
      for (i = 0; i < len-1; i++) 
        if (url[i] == '.' && url[i+1] == '.') {
          errno = EPERM;   /* permission denied */
          code = 403;
          path = NULL;
          break;
        }

      /* on retourne un 404 si l'url commence par '/' */
      if (url[0] == '/') {
        code = 404;
        path = NULL;
      }
#endif
      
      /* si tout est en ordre */      
      if (path) /* on ajoute l'url � la fin du path */
        strncat (path, url, path_len - strlen(path));
      
    } /* fin taille OK */
  } /* fin taille > 1 */
  
  
  /* ajustements au path */
  if (path) {

    struct stat fstatsBak;
    
    len = strlen(path);     /* len d�signe maintenant la taille du path */
    
    /* si le path se termine par "/." */
    if (path[len-1] == '.' && path[len-2] == '/') 
      path[--len] = '\0';   /* on termine avant le '.' */

    code = statFile(path, fstats);

#ifdef INDEX_FILE
    /* ./ -> ./index.html */
    
#ifdef USE_UTILS_H
    if (IsFileDirectory(path))
#else
      ;                         /* ; pour l'indentation */
    if (S_ISDIR(fstats->st_mode))
#endif
      {
        if (path[len-1] != '/') { /* on termine le path par '/' */
          path[len++] = '/';
          path[len] = '\0';
        }
        warnx("file \"%s\" is a directory, seeking \"%s%s\"",
              path, path, INDEX_FILE);
        strncat(path, INDEX_FILE, path_len - len); /* ajout du fichier d'index */
        
        /* acc�s au fichier d'index */
        
        if (statFile(path, &fstatsBak) == 200) {
          code = 200;
          memcpy(fstats, &fstatsBak, sizeof (struct stat));
        } else {
          path[len] = '\0';     /* remove it */
        }
      }
#endif
  } /* fin if path */
  
  warnx("url \"%s\" translated to \"%s\"", urlBak, path);
  
  return code;
  
} /* fin findResource() */

/***********************************************************************
 * Donne le response code d'un HEAD sur un fichier
 *
 * Si NULL est donn� comme path, on analyse errno pour d�terminer un
 * responseCode appropri�.
 *
 * Sinon, on fait un stat sur le path. Si il y a erreur, elle est
 * analys�e (errno). Des erreurs IO (EFAULT, EIO) retournent un code 500
 * (Server error). Les erreurs de permissions retourne 403 (Forbidden).
 * Les erreurs dans le path (ENOTDIR, ENAMETOOLONG, ENOENT, ELOOP)
 * retournent 404 (Not Found).
 * 
 * Les r�pertoires et les autres fichiers n'ayant pas S_IFREG comme mode
 * sont 403 (Forbidden).
 *
 * Retourne le code HTTP (200, 400, etc) ou -1 si une erreur s'est
 * produite dans writeHttpHeader().
 *
 * Le struct stat pass� en param�tre sera initialis� avec un appel
 * de stat(2) sur le path.
 **********************************************************************/
inline int statFile(char *path, struct stat *fstats) {

  int responseCode = 200;
  
  if (!path) {       /* erreur dans la traduction */
    
    switch (errno) {
    case EACCES:
    case EPERM:
      responseCode = 403;
      break;
    case ENAMETOOLONG: 
      responseCode = 404;
      break;
    default: /* case ENOMEM: */
      responseCode = 500;
      break;
    }
    
  } else {                      /* Bonne entr�e */
    
#ifdef HAVE_UTILS_H
    stat(path, fstats);         /* il faut faire un stat anyways pour trouver le type */
    if (!DoFileExists(path))    /* fichier non trouv� */
      responseCode = 404;
#else
    if (stat(path, fstats) < 0) { /* erreur dans le stat */
      
      switch (errno) {
        
      case ENOTDIR:             /* A component of the path prefix is not
                                   a directory. */
      case ENAMETOOLONG:        /* A component of a pathname exceeded
                                   255 characters, or an entire path
                                   name exceeded 1023 characters. */
      case ENOENT:              /* The named file does not exist. */
      case ELOOP:               /* Too many symbolic links were
                                   encountered in translating the
                                   pathname. */
        responseCode = 404;     /* Not Found */
        break;
      case EACCES:              /* Search permission is denied for a
                                   componnt of the path prefix. */
        responseCode = 403;     /* Forbidden */
        break;
        /* case EFAULT: */        /* Sb or name points to an invalid
           address. */
        /* case EIO: */           /* An I/O error occurred while reading
           from or writing to the file system.*/
      default:
        responseCode = 500;     /* Internal Server error */
        break;
      }
    }
#endif
    else {                    /* stat OK */
      
      if (S_ISREG(fstats->st_mode) || S_ISDIR(fstats->st_mode)) {
        if (access(path, R_OK) == 0) { /* access en lecture OK */
          responseCode = 200;   /* OK */
        } else {                /* no access */
          responseCode = 403;   /* Forbidden */
        }
      } else { /* on ignore les r�pertoires et les fichiers
                  non-r�guliers */
        responseCode = 404;     /* Not Found */
      }
    }
  }

  return responseCode;
}


/***********************************************************************
 * Retourne la partie apr�s le dernier slash de path
 *
 * Retourne NULL si on n'a pas pu allouer de la m�moire.
 *
 * Bas� sur dirname(1) sur FreeBSD (http://www.freebsd.org/):
 * "@(#)dirname.c	8.4 (Berkeley) 5/4/95"
 **********************************************************************/
char* dirname(char *path) {

  char *p = strdup(path);
  char *garbage = p;
  char *dir = malloc( (sizeof (char)) * (strlen(path) + 2));

  if (!dir) return NULL;         /* return NULL on ENOMEM */
  
  memset((void*) dir, '\0', strlen(path));
  path = p;
  
  /* ceci provient presque directement de dirname(1) sur FreeBSD
   * 3.4-stable */
  
  /*
   * (1) If string is //, skip steps (2) through (5).
   * (2) If string consists entirely of slash characters, string
   *     shall be set to a single slash character.  In this case,
   *     skip steps (3) through (8).
   */
  for (p = path; *p == '/'; ++p) {
    if (!*p) {
      if (p > path)
        strcat(dir, "/");
      else
        strcat(dir, ".");
      return dir;
    }
  }
  
  /*
   * (3) If there are any trailing slash characters in string, they
   *     shall be removed.
   */
  for (; *p; ++p);
  while (*--p == '/')
    continue;
  *++p = '\0';
  
  /*
   * (4) If there are no slash characters remaining in string,
   *     string shall be set to a single period character.  In this
   *     case skip steps (5) through (8).
   *
   * (5) If there are any trailing nonslash characters in string,
   *     they shall be removed.
   */
  while (--p >= path)
    if (*p == '/')
      break;
  ++p;
  if (p == path) {
    strcat(dir, ".");
    return dir;
  }
  
  /*
   * (6) If the remaining string is //, it is implementation defined
   *     whether steps (7) and (8) are skipped or processed.
   *
   * This case has already been handled, as part of steps (1) and (2).
   */
  
  /*
   * (7) If there are any trailing slash characters in string, they
   *     shall be removed.
   */
  while (--p >= path)
    if (*p != '/')
      break;
  ++p;
  
  /*
   * (8) If the remaining string is empty, string shall be set to
   *     a single slash character.
   */
  *p = '\0';
  strcat(dir, (p == path ? "/" : path));
  free(garbage);
  return dir;
  
} /* fin dirname() */

/***********************************************************************
 * Retire la partie "fichier" d'un path
 *
 * Retourne NULL si on ne peut pas allouer de m�moire
 *
 * Donne le nom du fichier sans le path pour y acc�der.
 * Tir� de:
 * @(#)basename.c	8.4 (Berkeley) 5/4/95
 **********************************************************************/
char* basename(char *path) {

  char *p = strdup(path);
  char *garbage = p;
  char *base = malloc( (sizeof (char)) * (strlen(path) + 2));

  if (!base) return NULL;

  memset((void*) base, '\0', strlen(path));
  
  path = p;

  /*
   * (1) If string is // it is implementation defined whether steps (2)
   *     through (5) are skipped or processed.
   *
   * (2) If string consists entirely of slash characters, string shall
   *     be set to a single slash character.  In this case, skip steps
   *     (3) through (5).
   */
  for (p = path;; ++p) {
    if (!*p) {
      if (p > path)
        strcat(base, "/");
      else
        strcat(base, ".");
      exit(0);
    }
    if (*p != '/')
      break;
  }

  /*
   * (3) If there are any trailing slash characters in string, they
   *     shall be removed.
   */
  for (; *p; ++p)
    continue;
  while (*--p == '/')
    continue;
  *++p = '\0';
  
  /*
   * (4) If there are any slash characters remaining in string, the
   *     prefix of string up to an including the last slash character
   *     in string shall be removed.
   */
  while (--p >= path)
    if (*p == '/')
      break;
  ++p;
  
  /*
   * (5) If the suffix operand is present, is not identical to the
   *     characters remaining in string, and is identical to a suffix
   *     of the characters remaining in string, the suffix suffix
   *     shall be removed from string.
   */
  if (*path) {
    int suffixlen, stringlen, off;
    
    suffixlen = strlen(path);
    stringlen = strlen(p);
    
    if (suffixlen < stringlen) {
      off = stringlen - suffixlen;
      if (!strcmp(p + off, path))
        p[off] = '\0';
    }
  }
  strcat(base, p);
  free(garbage);
  return base;
}

/***********************************************************************
 * Cherche un "query string" (...?<str>) dans l'url donn�.
 *
 * Retourne un pointeur vers la position du d�but de la cha�ne et modifie
 * la cha�ne donn� pour qu'elle se termine au lieu du '?', ou NULL si
 * aucun ? est trouv�
 ***********************************************************************/
char* query_string(char* url) {

  int i, len = strlen(url);
  for (i = 0; i < len; i++) {
    if (url[i] == '?') {
      url[i] = '\0';
      return &url[i+1 < len ? i+1 : i];
    }      
  }
  return NULL;
} /* fin query_string */

#define UNDEF "undefined"

/**
 * L'index de "content-type"
 *
 * Index� avec les macros de misc.h
 **/
const char *C_STRING[128] =
{"text", "plain", "richtext", "enriched", "html", "sgml", "rfc1822",
 "css", "xml", "rtf", "directory", "calendar", UNDEF, UNDEF, UNDEF, UNDEF,
 "multipart", "mixed", "alternative", "digest", "parallel", "appledouble", 
 "header-set", "form-data", "related", "report", "voice-message", "signed",
 "encrypted", "byteranges", UNDEF, UNDEF,
 "message", "rfc822", "partial", "external-body", "news", "http", "delivery-status",
 "disposition-notification", "s-http", UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,
 "application", "octet_stream", "postscript", "oda", "news_message_id",
 "news_transmission", "pdf", "zip", "msword", "mathematica", "pgp_encrypted", 
 "pgp_signature", "pgp_keys", "pkcs7_mime", "pkcs7_signature", "pkcs10",
 "image", "jpeg", "gif", "ief", "g3fax", "tiff", "cgm", "naplps", "png",
 UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,
 "audio", "basic", "32kadpcm", "l16", UNDEF, UNDEF, UNDEF, UNDEF,
 UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,
 "video", "mpeg", "quicktime", UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,
 UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, 
 "model", "iges", "vrml", "mesh", UNDEF, UNDEF, UNDEF, UNDEF,
 UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF
};

/***********************************************************************
 * Determine le 'content-type' du fichier point� par path et d�crit par
 * fstats
 **********************************************************************/
int content_type(char *path, struct stat *fstats) {

  int i; int type = C_PLAIN; /* text/plain par d�faut */

  if (fstats && (S_ISDIR(fstats->st_mode))) /* r�pertoires */
    type = C_HTML;            /* listings text/html */
  else {
    /* on cherche le point */
    for (i = strlen(path); i > 0 && path[i] != '.'; i--);

    if (path[i] == '.') {     /* si on le trouve */
      char *p = &path[i+1];   /* on compare avec p */
      /* si nous aurions un autre tableau contenant les extensions
       * nou pourrions faire de ceci une boucle */
      if (!strcmp(p, "htm") || !strcmp(p, "html"))
        type = C_HTML;
      else if (!strcmp(p, "txt") || !strcmp(p, "c"))
        type = C_PLAIN;
      else if (!strcmp(p, "gif"))
        type = C_GIF;
      else if (!strcmp(p, "jpg") || !strcmp(p, "jpeg"))
        type = C_JPEG;
      else if (!strcmp(p, "pdf"))
        type = C_PDF;
      else if (!strcmp(p, "cgi"))
        type = C_CGI;
    } else                    /* pas de point */
      type = C_PLAIN;         /* text/plain */
  }

  return type;
}
