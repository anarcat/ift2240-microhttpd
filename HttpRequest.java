////////////////////////////////////////////////////////////////////////
// Module de communication HTTP/1.0
////////////////////////////////////////////////////////////////////////
// @author The AnarCat
// @version $Id: HttpRequest.java,v 0.6 2000-04-12 15:07:11-04 spidey Exp spidey $
////////////////////////////////////////////////////////////////////////

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

////////////////////////////////////////////////////////////////////////
// Requ�te HTTP 1.0
//
// Une requ�te doit �tre initialis� avec le constructeur  
////////////////////////////////////////////////////////////////////////
class HttpRequest extends Object {

    protected static String version = "HTTP/1.0";
    protected Socket sock;
    protected InputStream sock_in;
    protected OutputStream sock_out;
    protected BufferedReader in;
    
    ////////////////////////////////////////////////////////////////////
    // Implantation des m�thodes de InputStream comme des appels �
    // sock_in
    ////////////////////////////////////////////////////////////////////
    
    // Constructeur par d�faut
    //
    // Essaie localhost:80 comme host
    public HttpRequest ()
        throws UnknownHostException, IOException
    {this("localhost", 80);}

    // Essaie le hostname donn�
    public HttpRequest (String hostname, int port)
        throws UnknownHostException, IOException
    {       
        super();
        sock = new Socket(hostname, port);
        sock_in = sock.getInputStream();
    }
    
    public String get(String url, boolean getHead) throws IOException
    {
        PrintWriter req = new PrintWriter
            (sock_out = sock.getOutputStream());
        req.print("GET " + url + " " + getVersion() + "\r\n\r\n");
        req.flush();
        
        String message = "";
        String buffer;
        
        buffer = readChunk();
        
        if (getHead)            // if we keep the head
            message += buffer; // add it to message
        
        while ( (buffer = readChunk()) != null) // read the rest
            message += buffer;
        
        return message;
    }

    public String head(String url) throws IOException
    {
        PrintWriter req = new PrintWriter
            (sock_out = sock.getOutputStream());
        req.print("HEAD " + url + " " + getVersion() + "\r\n\r\n");
        req.flush();
        
        return readChunk();
        
    }

    public void close() throws IOException
    {        
        sock_out.close();
        sock_in.close();
        sock.close();
    }  

    ////////////////////////////////////////////////////////////////////
    // Read from socket until CRLFCRLF (empty line) or eof
    //
    // Return null if eof without char read
    ////////////////////////////////////////////////////////////////////
    private String readChunk() throws IOException
    {
        String buffer;
        String file = "";
        boolean read = false;

        if (in == null)
            in = new BufferedReader
                (new InputStreamReader(sock_in));

        while ( (buffer = in.readLine()) != null) {
            read = true;
            file += buffer + '\n';
            if (buffer.equals("")) break; // stop reading at empty line
        }
        
        if ( !read ) {
            
            return null;
        } else return file;
        
    }
    
    ////////////////////////////////////////////////////////////////////
    // M�thodes statiques
    ////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////
    // Retourne la version de l'implantation
    public static String getVersion() {return version;}
}
