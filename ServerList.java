////////////////////////////////////////////////////////////////////////
// La liste des serveurs et les proc�dures pour la mettre � jour
////////////////////////////////////////////////////////////////////////
// @author The AnarCat
// @version $Id: ServerList.java,v 0.7 2000-04-12 18:24:41-04 spidey Exp spidey $
////////////////////////////////////////////////////////////////////////

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

////////////////////////////////////////////////////////////////////////
// La liste de serveurs
//
// Contient les hostnames, les ports, et les noms symboliques des hosts
////////////////////////////////////////////////////////////////////////
class ServerList extends List {

    private int servCnt;
    private int maxServCnt;
    private String hostList[];
    private int portList[];

    ////////////////////////////
    // Constructeur de classe //
    ////////////////////////////
    public ServerList() {

        super(3, false);        // 3 lignes, selection unique
        servCnt = 0;            // aucun serveur a date
        maxServCnt = 30;        // nombre max initial
        hostList = new String[maxServCnt];
        portList = new int[maxServCnt];
        seekServers();          // on cherche les serveurs initiaux
                
    }
    
    public String getHost() {

        int index = getSelectedIndex();
        if (index < 0) return null;
        return hostList[index];

    }

    public int    getPort() {

        int index = getSelectedIndex();
        if (index < 0) return -1;
        return portList[index];

    }

    /////////////////////////////////////////////////////
    // Ajoute le serveur donne a la fin de la liste
    //
    // Appelle expandLists() si necessaire
    /////////////////////////////////////////////////////
    public void append(String name, String hostname, int port) {

        if (servCnt >= maxServCnt-1) expandLists(); // expansion
        hostList[servCnt] = hostname;
        portList[servCnt] = port;
        add(name);
        servCnt++;

    }

    //////////////////////////////////////////////////////
    // Double la taille des tables
    //////////////////////////////////////////////////////
    private void expandLists() {

        maxServCnt *= 2; // on double la taille maximum
        String tmpHostList[] = new String[maxServCnt];
        int    tmpPortList[] = new int   [maxServCnt];

        for (int i = 0; i < maxServCnt/2; i++) {

            tmpHostList[i] = hostList[i];
            tmpPortList[i] = portList[i];

        }

        // la garbage collector fera le menage...
        hostList = tmpHostList;
        portList = tmpPortList;
        
    }
    
    //////////////////////////////////////////////////////////////////
    /// Reg�n�re la liste de serveurs
    //////////////////////////////////////////////////////////////////
    public void seekServers() {
        
        String logins;
        HttpRequest req = null;
        String path = "";

        try {
            req = new HttpRequest("www.iro.umontreal.ca", 80);
            logins = req.get("/~dift2240/2240_liste", false); // only get body
            req.close();

            for (int i = 0; // on it�re sur la liste
                 i < logins.length(); i++) {
                int begl = i;   // d�but d'une ligne
                int endl;       // fin de la ligne
                String login;
                
                // on cherche la fin de la ligne
                for (endl = begl;
                     endl < logins.length() &&
                         logins.charAt(endl) != '\n'; endl++);
                login = logins.substring(begl, endl);

                // fin de la ligne ou de la string
                String hostStr = "";
                try {
                  
                    int port, j;
                    String portStr;
                  
                  req = new HttpRequest("www.iro.umontreal.ca", 80);
                  path = "/~" + login + "/2240_Serveur";
                  hostStr = req.get(path, false);

                  for (j = 0; j < hostStr.length(); j++)
                      if (hostStr.charAt(j) == ':') break; // found sep
                  portStr = hostStr.substring(j+1, hostStr.length()-1);
                  port = Integer.parseInt(portStr);

                  System.out.println
                      ("Adding server " + hostStr.substring(0,j)
                       + ":" + port + " as " + login);
                  
                  append(login, hostStr.substring(0, j), port);
                  
                  req.close();
                } catch (IOException exc) {
                  System.err.println
                    ("Can't read url: http://www.iro.umontreal.ca:80" + path);
                } catch (NumberFormatException exc) {
                    System.err.println
                      ("Error in file: " + path + ", content: " + hostStr + exc);
                } catch (StringIndexOutOfBoundsException exc) {
                }
                i = endl;
            }
                
            
        } catch (IOException exc) {
            System.err.println
                ("Can't read server list from www.iro.umontreal.ca:80");
        } finally {
            try {if (req != null) req.close();}
            catch (IOException exc) {
                System.err.println("Error in closing request"
                                   + exc.getMessage());
            }
            System.err.println("Adding default servers");
            append("Local - private", "localhost", 50080);
            append("Local - public", "localhost", 80);
            append("DIRO", "www.iro.umontreal.ca", 80);
            append("DIRO2", "www2.iro.umontreal.ca", 80);
        }

    }
    
}
