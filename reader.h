/****************************-*-C-*-***********************************
 * $Id: reader.h,v 2.1 2000-04-12 05:00:06-04 spidey Exp spidey $
 **********************************************************************
 * Proc�dures de lecture HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef _READER_H
#define _READER_H

/***********************************************************************
 * Protocole HTTP/1.1 lecture
 **********************************************************************/

/***********************************************************************
 * Read Request-line and headers from clientfd
 *
 * Ceci saute les \r ou \n initiaux et arr�te la lecture apr�s CRLFCRLF,
 * ou si read() retourne 0 ou -1.
 *
 * Retourne une cha�ne nouvellement allou�e avec malloc(2).
 * Retourne NULL si malloc(2) �choue, ou si read() �choue. 
 **********************************************************************/
char* readHeader(int clientfd);

/***********************************************************************
 * Read the body of a message of a specified length
 *
 * Returns the number of bytes read
 **********************************************************************/
int readBody(int clientfd, char* body, int content_length);

/***********************************************************************
 * Traite une requ�te HTTP sur clientfd
 **********************************************************************/
int readRequest(int clientfd);

#endif
