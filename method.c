/****************************-*-C-*-***********************************
 * $Id: method.c,v 2.6 2001/10/01 05:52:24 anarcat Exp $
 **********************************************************************
 * Implantation des methodes HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include "params.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "method.h"

#include "writer.h"
#include "misc.h"

/***********************************************************************
 * Implantation de la m�thode HEAD
 *
 * Appelle writeHttpHeader() avec le code et stat donn�.
 *
 * Retourne -1 si une erreur s'est produite dans writeHttpHeader(), 0
 * sinon.
 **********************************************************************/
int headRequest(int clientfd,   /* le descripteur sur lequel �crire */
                char *path,
                int code,       /* le code de r�ponse */
                struct stat *fstats, /* le r�sultat d'un stat(2) */
                char* query_string) {
  
  if (writeHttpHeader(clientfd, path, code, fstats) < 0)
    return -1;
  else
    return 0;

} /* fin headRequest() (HEAD method) */

/****************************************************************************
 * Implantation de la m�thode GET
 *
 * Appelle writeFile(), writeDir() ou writeCGI() si le code est 2xx et
 * path != NULL. Appelle writeCode() sinon ou si une erreur s'est produite.
 *
 * Retourne 0 si tout va bien ou -1 si une erreur en �criture s'est produite
 ***************************************************************************/
int getRequest(int clientfd,
               char* url, 
               char* path,
               int code,       /* le code de r�ponse */
               struct stat *fstats, /* le r�sultat d'un stat(2) */
               char* query_string) {

  int ret = 0;

  if (writeHttpHeader(clientfd, path, code, fstats) < 0)
    ret = -1;

  if (code < 0)       /* header failed */
    ret = -1;
  else if (path && code >= 200 && code < 300) {
    if (S_ISREG(fstats->st_mode)) { /* fichiers r�guliers */
      
      int r;
      if (content_type(path, fstats) == C_CGI)
        r = writeCGI(clientfd, url, path, query_string);
      else
        r = writeFile(clientfd, path);
      
      if (r < 0) {    /* error */
        ret = r;     /* record error */
        writeCode(clientfd, code);
      }
            
    } else if (S_ISDIR(fstats->st_mode)) { /* r�pertoire */
      
      writeDir(clientfd, path);
      
    } else writeCode(clientfd, code); /* autre fichiers */
  } else writeCode(clientfd, code); /* erreur ou code != 2xx */
  
  return ret;

} /* fin getRequest() */
