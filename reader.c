/****************************-*-C-*-***********************************
 * $Id: reader.c,v 2.3 2000/04/13 15:59:47 spidey Exp $
 **********************************************************************
 * Proc�dures de lecture HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <err.h>

#include "reader.h"

#include "params.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "writer.h"
#include "parser.h"
#include "misc.h"
#include "method.h"
#include "serveur.h"

/***********************************************************************
 * Protocole HTTP/1.1 lecture
 **********************************************************************/

/***********************************************************************
 * Read Request-line and headers from clientfd
 **********************************************************************/
char* readHeader(int clientfd) {

  int maxlen = BUFSIZ;      /* la taille initiale du buffer */
  char *buffer = (char*) malloc( (maxlen + 1) * sizeof(char)),
    *tmpBuf;
  int i, readBytes = 0;

  if (!buffer) return buffer;
  
  /* On lit le contenu de la requ�te, dans un seul buffer */
  while ( (i = read( clientfd, buffer + readBytes, 1 )) == 1 ) {

    readBytes++;
    
    if (1 == readBytes)
      /* RFC 2616 dit qu'il faudrait sauter les CRLF initiaux */
      if ((buffer[0] == '\r') || (buffer[1] == '\n'))
        readBytes = 0;      /* on est plus g�n�reux */
    
    /* si on trouve CRLF, on arr�te de lire */
    if ((readBytes > 4) &&
        !strncmp(buffer + readBytes - 4, "\r\n\r\n", 4))
      break;

    /* si le buffer devient trop petit, on l'agrandit.
     *
     * ceci devrait �tre limit� � une certaine taille pour �viter un DOS
     */
    if (readBytes + 2 >= maxlen) {

      maxlen *= 2;
      tmpBuf = (char*) malloc( (maxlen + 1) * (sizeof(char)));
      strncpy(tmpBuf, buffer, readBytes);
      free(buffer);
      buffer = tmpBuf;
      
    } /* fin de l'agrandissement du buffer */
    
  } /* fin lecture */

  /* on termine la chaine  */
  buffer[readBytes] = '\0';

  if (i < 0)
    free(buffer);
  return buffer;
  
} /* end readHeader() */

/***********************************************************************
 * Read the body of a message of a specified length
 *
 * Returns the number of bytes read
 **********************************************************************/
int readBody(int clientfd, char* body, int content_length) {

  int readBytes = 0;

  /* we should also decide here if we drain this through a temporary
   * file if the input is too big */
  if (content_length > 0) {

    /* On lit le contenu de la requ�te, dans un seul buffer */
    readBytes = read( clientfd, body, content_length );
    if (readBytes < 0) return -1;

  }
  
  body[readBytes] = '\0';
  return readBytes;

}

/***********************************************************************
 * Traite une requ�te HTTP sur clientfd
 **********************************************************************/
int readRequest(int clientfd) {
  
  int req;                  /* le code de requ�te */
  char *url,                /* l'url demand� */
    path[MAXPATHLEN];       /* le path de l'url */
  char *version, *request, *urlTok;
  char *header;             /* buffer de lecture du stream */
  char *tmpHead;
  char *body;               /* message body, if any */
  struct stat fstats;
  int code;                 /* response code HTTP */
  char *query;              /* query string */

  clientVersion = HTTP_0_9;     /* on assume un client HTTP/0.9 */

  header = readHeader(clientfd); /* on lit le header */
  
  if (header == NULL) {
    warn("error in reading message header from client");
    return -1;
  }
  
  /* lecture des tokens */
  warnx("reading tokens from header: \"%s\"", header ); /* on l'imprime */
  
  tmpHead = header;         /* pointeur de suivi */

  request = nextToken(&tmpHead);
  warnx("parsed request: %s", request);
  
  if (request && *tmpHead) {
    urlTok = nextToken(&tmpHead);
    warnx("parsed url: %s", urlTok);

    if (urlTok && *tmpHead) {
      version = nextToken(&tmpHead);
      warnx("parsed version: %s", version);
    }
  }

  /* parseHeaders(tmpHead); */

  /* lecture du body du message */
  body = (char*) malloc( (/* content_length + */ 1) *
                         sizeof(char));
  
  readBody(clientfd, body, /* content_length */ 0);
  
  
  /* analyse */
  if ((!request) || (!urlTok)) {
    writeErrorCode(clientfd, 400);
  } else {                  /* url trouv� */
    
    url = parseUrl(urlTok);

    if ((!url) || (clientVersion & HTTP_ERROR_VER))
      writeErrorCode(clientfd, 400);
    else {
      /* analyse de la version du client */
      clientVersion = parseVersion(version); 
      req = parseMethod(request); /* analyze de la m�thode HTTP */
      
      query = query_string(url); /* on trouve le query_string */
      
      /* Interpr�tation de l'url en un path
       *
       * Retourne NULL si l'url contient ".." (EPERM), s'il est trop long,
       * (ENAMETOOLONG) ou si la m�moire n'a pas pu �tre allou�e (ENOMEM).
       */
      code = findResource(url, path, MAXPATHLEN, &fstats);

      if (req & HTTP_HEAD)     {
        warnx("HEAD request for path: \"%s\"", path);
        headRequest(clientfd, path, code, &fstats, query);
      } else if (req & HTTP_GET) {
        warnx("GET request for path: \"%s\"", path);
        getRequest(clientfd, url, path, code, &fstats, query);
      } else if (req & HTTP_UNKNOWN) {
        char body[150];
        warnx("unknown method");
        snprintf(body, 150, "Method %s not defined in %s standard. This\
 is probably an programming error in your browser.", request,
                 (clientVersion & (HTTP_0_9 | HTTP_ERROR_VER | HTTP_UNKNOWN_VER) ?
                  "HTTP/0.9" :  version));
        writeHttpHeader(clientfd, NULL, 400, NULL);
        writeHtmlPage(clientfd, "Unknown method", body);
      } else if (req & HTTP_ERROR) {
        warnx("bad request: %s", request);
        writeErrorCode(clientfd, 400); /* erreur du client */
      } else {
        warnx("method not implemented");
        writeErrorCode(clientfd, 501);
      }
    } /* url avec / au d�but ou version client correcte */
  } /* fin url trouv� */
  free(header);
  free(body);
  return 0;
  
} /* fin readRequest() */
