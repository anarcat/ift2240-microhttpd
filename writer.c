/****************************-*-C-*-***********************************
 * $Id: writer.c,v 2.8 2001/10/01 05:52:24 anarcat Exp $
 **********************************************************************
 * Proc�dures d'�criture de HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include <err.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>

#include "writer.h"

#include "misc.h"
#include "params.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "serveur.h"

/***********************************************************************
 * Protocole HTTP/1.1, �criture
 **********************************************************************/

/***********************************************************************
 * �crit un header HTTP de r�ponse avec code (200, 404..) sur le
 * descripteur fd
 * 
 * Si la valeur de httpCode est inconnue ou hors limite, on fait un 500
 *
 * On peut passer en param�tre un struct stat (voir stat(2)) pour avoir
 * header plus complet.
 ***********************************************************************/
int writeHttpHeader(int clientfd,
                    char *path,
                    int httpCode,                    
                    struct stat* fstats) {

  char *cur_time;                     /* la date de la requ�te */
  struct timeval timeValue;
  struct tm* gmtime_p;
  char *message;
  
  warnx("writeHttpHeader: code %d, file \"%s\"", httpCode, path);
  /**
   * On r�pond la avec la version repr�sentant les capacit�s du serveur
   *
   * Voir RFC 2145
   **/
  if (write(clientfd, "HTTP/1.1 ", 9) < -1) return -1;
  
  switch (httpCode) {           /* interpr�tation du code selon RFC 2616 */
  case 100:                     /* Continue */
    message = "100 Continue";
    break;
  case 101:                     /* Switching Protocols */
    message = "101 Switching Protocols";
    break;
  case 200:                     /* OK */
    message = "200 OK";
    break;
  case 201:                     /* Created */
    message = "201 Created";
    break;
  case 202:                     /* Accepted */
    message = "202 Accepted";
    break;
  case 203:                     /* Non-Authoritative Information */
    message = "203 Non-Authoritative Information";
    break;
  case 204:                     /* No Content */
    message = "204 No Content";
    break;
  case 205:                     /* Reset Content */
    message = "205 Reset Content";
    break;
  case 206:                     /* Partial Content */
    message = "206 Partial Content";
    break;
  case 300:                     /* Multiple Choices */
    message = "300 Multiple Choices";
    break;
  case 301:                     /* Moved Permanently */
    message = "301 Moved Permanently";
    break;
  case 302:                     /* Found */
    message = "302 Found";
    break;
  case 303:                     /* See Other */
    message = "303 See Other";
    break;
  case 304:                     /* Not Modified */
    message = "304 Not Modified";
    break;
  case 305:                     /* Use Proxy */
    message = "305 Use Proxy";
    break;
  case 307:                     /* Temporary Redirect */
    message = "307 Temporary Redirect";
    break;
  case 400:                     /* Bad Request */
    message = "400 Bad Request";
    break;
  case 401:                     /* Unauthorized */
    message = "401 Unauthorized";
    break;
  case 402:                     /* Payment Required */
    message = "402 Payment Required";
    break;
  case 403:                     /* Forbidden */
    message = "403 Forbidden";
    break;
  case 404:                     /* Not Found */
    message = "404 Not Found";
    break;
  case 405:                     /* Method Not Allowed */
    message = "405 Method Not Allowed";
    break;
  case 406:                     /* Not Acceptable */
    message = "406 Not Acceptable";
    break;
  case 407:                     /* Proxy Authentication Required */
    message = "407 Proxy Authentication Required";
    break;
  case 408:                     /* Request Time-out */
    message = "408 Request Time-out";
    break;
  case 409:                     /* Conflict */
    message = "409 Conflict";
    break;
  case 410:                     /* Gone */
    message = "410 Gone";
    break;
  case 411:                     /* Length Required */
    message = "411 Length Required";
    break;
  case 412:                     /* Precondition Failed */
    message = "412 Precondition Failed";
    break;
  case 413:                     /* Request Entity Too Large */
    message = "413 Request Entity Too Large";
    break;
  case 414:                     /* Request-URI Too Large */
    message = "414 Request-URI Too Large";
    break;
  case 415:                     /* Unsupported Media Type */
    message = "415 Unsupported Media Type";
    break;
  case 416:                     /* Requested range not satisfiable */
    message = "416 Requested range not satisfiable";
    break;
  case 417:                     /* Expectation Failed */
    message = "417 Expectation Failed";
    break;
  case 501:                     /* Not Implemented */
    message = "501 Not Implemented";
    break;
  case 502:                     /* Bad Gateway */
    message = "502 Bad Gateway";
    break;
  case 503:                     /* Service Unavailable */
    message = "503 Service Unavailable";
    break;
  case 504:                     /* Gateway Time-out */
    message = "504 Gateway Time-out";
    break;
  case 505:                     /* HTTP Version not supported */
    message = "505 HTTP Version not supported";
    break;
  default:                      /* autres codes sont: */
  case 500:                     /* Internal Server Error */
    message = "500 Internal Server Error";
    break;
  }

  if ( (write(clientfd, message, strlen(message)) < 0) ||
       (write(clientfd, "\r\n", 2) < 0) )
    return -1;
  

  if (httpCode == 405)
    if (write(clientfd, "Allow: HEAD, GET\r\n", 18) < 0) 
      return -1;

  gettimeofday(&timeValue, NULL);
  gmtime_p = gmtime(&timeValue.tv_sec);
  cur_time = asctime(gmtime_p);
  cur_time[strlen(cur_time) - 1] = '\r';
  
  if (
      (write(clientfd, "Date: ", 6) < 0) ||
      (write(clientfd, cur_time, strlen(cur_time)) < 0) ||
      (write(clientfd, "\n", 1) < 0) ||
      (write(clientfd, "Accept-Ranges: none\r\n", 21) < 0) || /* not implemented yet */
      (write(clientfd, "Server: ", 8) < 0) ||
      (write(clientfd, SERVERNAME, strlen(SERVERNAME)) < 0) ||
      (write(clientfd, "\r\n", 2) < 0) ||
      (write(clientfd, "Allow: GET\r\n", 12) < 0) ||
      (write(clientfd, "Connection: close\r\n", 19) < 0)
      ) return -1;
  
  if (httpCode == 200 && fstats) { /* si la resource existe, on lit les stats */
    char size[BUFSIZ];
    snprintf(size, BUFSIZ, "%li", (long int)fstats->st_size);

    /* cur_time repr�sente maintenant la date de modification du fichier */
    cur_time = asctime(gmtime(&fstats->st_mtime));
    cur_time[strlen(cur_time) - 1] = '\r';
    
    if (
        (write(clientfd, "Last-Modified: ", 15) < 0) ||
        (write(clientfd, cur_time, strlen(cur_time)) < 0) ||
        (write(clientfd, "\n", 1) < 0) ||
        (write(clientfd, "Content-Length: ", 16) < 0) ||
        (write(clientfd, size, strlen(size)) < 0) ||
        (write(clientfd, "\r\n", 2) < 0)
        ) return -1;

  }

  /**
   * on analyse le content-type d'apr�s l'extension:
   * .html ou .htm ( retourner text/html).
   * .txt ou .c    ( ... text/html)
   * .gif          ( ... image/gif).
   * .jpg ou jpeg  ( ... image/jpeg).
   * .pdf          ( ... application/pdf).
   * .cgi      ( EXECUTER le fichier et retourner son output).
   **/
  if (httpCode < 300 && httpCode >= 200 && path && fstats) {
    /* si le fichier est trouv� */
    int type = content_type(path, fstats);
    
    if (type != C_CGI)          /* impriment eux-m�me content-type */
      if ((write(clientfd, "Content-Type: ", 14) < -1) ||
          (write(clientfd, C_STRING[type & C_FAMILY], /* famille */
                 strlen (C_STRING[type & C_FAMILY])) < -1) ||
          (write(clientfd, "/", 1) < -1) || /* /type */
          (write(clientfd, C_STRING[type], strlen(C_STRING[type])) < -1) ||
          (write(clientfd, "\r\n\r\n", 4) < -1)) /* s�parateur */
        return -1;
  } else {
    /* fichier non trouv� (code != 2XX) */
    if (write(clientfd, "Content-Type: text/html\r\n\r\n", 27) < -1)
      return -1;
    
  }
  
  return 0;

} /* fin writeHttpHeader() */

/***********************************************************************
 * Imprime une page HTML sur le descripteur donn�.
 *
 * On peut sp�cifier un titre NE CONTENANT PAS DE HTML, et un body
 * pouvant contenir du html. Il est important de noter que le 'body'
 * sera ins�r� entre 2 series de tags "<body>" - "</body></html>" pour
 * �viter les erreurs HTML.
 *
 * Si title est NULL, "" est �crit comme title, et si body est NULL, une
 * page vide est envoy�e seulement si title est NULL. Sinon le title est
 * reproduit dans le body.
 **********************************************************************/
int writeHtmlPage(int clientfd, char* title, char* body) {

  if (!title) {                 /* si title == NULL */
    title = "";
    if (!body)                  /* si body == NULL */
      body = "";
  } else                        /* si title != NULL */
    if (!body)                  /* si body == NULL */
      body = title;
      
  /* on v�rifie pour les erreurs */
  if (
      (write(clientfd, "<html>\r\n<head>\r\n<title>",23 ) < 0) ||
      (write(clientfd, title, strlen(title)) < 0) || 
      (write(clientfd, "</title>\r\n</head>\r\n<body>\r\n", 27 ) < 0) ||
      (write(clientfd, body, strlen(body)) < 0) ||
      (write(clientfd, "\r\n</body>\r\n</html>\r\n", 20) < 0)
      )
    return -1;
  else return 0;
  
} /* fin writeHtmlPage() */

/***********************************************************************
 * Imprime une le flux d�crit pas fd sur clientfd
 *
 * read() des tampons de taille BUFSIZ sur fd et write() sur clientfd.
 * Retourne le nombres d'octets �crits sur clientfd ou -1 si une erreur
 * s'est produite.
 *
 * Ceci sera en g�n�ral utilis� pour des requ�tes GET.
 **********************************************************************/
int writeStream(int clientfd, int fd) {

  char buffer[BUFSIZ];
  int readBytes, wroteBytes, totalBytes = 0;

  while ( (readBytes = read(fd, buffer, BUFSIZ)) > 0) {
    wroteBytes = write(clientfd, buffer, readBytes);
    if (wroteBytes < 0)
      return -1;
    totalBytes += wroteBytes;
  }

  if (readBytes < 0) return -1;
  else return totalBytes;
  
} /* fin writeFile() */

/***********************************************************************
 * �crit le fichier donn� par path sur clientfd
 *
 * Appelle writeStream() apr�s avoir ouvert le fichier.
 *
 * Retourne -1 si writeStream() �choue, -2 si le fichier n'a pas pu
 * �tre ouvert, le nombre de bytes �crits sinon.
 **********************************************************************/
int writeFile(int clientfd, char *path) {

  int ret = 0;
  /* il faut lire le fichier, donc l'ouvrir */
  int fd = open(path, O_RDONLY);
  
  if (fd > 0) {               /* open() r�ussi */
    if ( (ret = writeStream(clientfd, fd)) < 0 ) /* on l'�crit sur le socket */
      warn("writeFile: error writing file \"%s\" to client", path);
    else
      warnx("writeFile: written %d bytes of file \"%s\" to client",
            ret, path);
    close(fd);
  } else {                    /* open() �chou� */
    warn("writeFile: can't open file \"%s\"", path);
    ret = -1;
  } /* fin open() �chou� */
  return ret;
  
}

/***********************************************************************
 * �crit le contenu du r�pertoire sur clientfd
 *
 * Retourne le nombre de bytes �crits
 **********************************************************************/
int writeDir(int clientfd, char* path) {

  DIR *directory;               /* le r�pertoire */
  struct dirent* entry;         /* une entr�e de r�pertoire (les
                                 * fichiers � lire) */
  char *begin, *end, *begline, *endline;
  int beglen, endlen, wroteBytes = 0;
  directory = opendir(path);    /* on ouvre le r�pertoire en argument */
  
  if (directory == NULL) {
    errno = ENOMEM;
    return -1;
  }

  warnx("writeDir: creating listing for file \"%s\"", path);
  
  begin = "<html><head><title>Directory listing</title></head><body>\n\
<h1>Directory listing</h1><hr>";
  end = "</body></html>";
  wroteBytes += write(clientfd, begin, strlen(begin));
  
  begline = "<a href=\"";
  endline = "</a><br>\r\n";
  beglen = strlen(begline);
  endlen = strlen(endline);
  
  while ((entry = readdir(directory) ) != NULL) {

    int len = strlen(entry->d_name);
    
    wroteBytes += write(clientfd, begline, beglen);
    wroteBytes += write(clientfd, entry->d_name, len);
    if (entry->d_type == DT_DIR)
      wroteBytes += write(clientfd, "/", 1);
    wroteBytes += write(clientfd, "\">", 2);
    wroteBytes += write(clientfd, entry->d_name, len);
    if (entry->d_type == DT_DIR)
      wroteBytes += write(clientfd, "/", 1);
    wroteBytes += write(clientfd, endline, endlen);
    
  }

  closedir(directory);

  wroteBytes += write(clientfd, end, strlen(end));

  warnx("writeDir: written %d bytes to client for listing of \"%s\"",
        wroteBytes, path);
  return wroteBytes;

} /* fin writeDir() */

/***********************************************************************
 * �xecute le script point� par path et redirige la sortie vers clientfd
 *
 * Attention! Ceci fait un chdir(dirname(path))
 *
 * Ceci ne lit pour l'instant pas de donn�es sur clientfd comme cela se
 * devrait dans le cas de requ�tes POST. (un dup2() devrait suffire)
 **********************************************************************/
int writeCGI(int clientfd, char *url, char *path, char* query_string) {

  int pid, status;
  char *argv[2], *envp[6], *dir, *bin; /* le nouveau processus */

  if (!path) {                  /* si NULL est donn� comme path */
    errno = EFAULT;             /* 'bad address' */
    return -1;                  /* sortie avec erreur */
  }

  dir = dirname(path);          /* le r�pertoire o� il faut se d�placer */
  bin = basename(path);         /* on appelle le CGI sans son path */
  
  argv[0] = bin;                /* path au fichier */
  argv[1] = NULL;               /* pas d'autres arguments */

  /* Initialisation de l'environnement */
  /* QUERY_STRING */
  envp[0] = (char*) malloc(sizeof(char*) *
                           ( (query_string ? strlen(query_string) : 1) +
                            sizeof("QUERY_STRING=") + 2));
  strcat(envp[0], "QUERY_STRING=");
  strcat(envp[0], (query_string ? query_string : "?"));

  /* REQUEST_METHOD */
  envp[1] = "REQUEST_METHOD=GET";

  /* REMOTE_ADDR */
  envp[2] = (char*) malloc(sizeof(char*) *
                           (strlen(ip) +
                            sizeof("REMOTE_ADDR=") + 2));
  strcat(envp[2], "REMOTE_ADDR=");
  strcat(envp[2], ip);

  /* GATEWAY_INTERFACE */
  envp[3] = "GATEWAY_INTERFACE=CGI/1.1";

  /* SERVER_SOFTWARE */
  envp[4] = (char*) malloc(sizeof(char*) *
                           (sizeof("SERVER_SOFTWARE=") +
                            sizeof(SERVERNAME) + 2));
  strcat(envp[4], "SERVER_SOFTWARE=");
  strcat(envp[4], SERVERNAME);

  /**
   * Il manque les variables PATH_INFO et PATH_TRANSLATED ici.
   *
   * PATH_INFO est .../cgi-bin/script.cgi/path/info/to/file.ext
   *                                     ^^^^^^^^^^^^^^^^^^^^^^
   *                                          Cette partie
   *
   * PATH_TRANSLATED est le path vers le fichier PATH_INFO une fois
   * interpr�t� comme un url
   *
   * Pour les avoir, il faudrait �tre capable de d�tecter un fichier CGI
   * avant la fin de l'url, et passer cette info en param�tre
   **/

  /* fin envp */
  envp[5] = NULL;
  
  dup2(clientfd, STDOUT_FILENO); /* redirect STDOUT to clienfd */
  /* we do not yet redirect STDIN through clientfd, because the CGI
     input is eaten by the read() call readRequest() */
  /* dup2(clientfd, STDIN_FILENO); */
  
  /* on se d�place dans le r�pertoire du CGI */
  if (chdir(dir) < 0) {
    warn("CGI execve: chdir(\"%s\") failed, aborting CGI", dir);
    free(dir);                  /* fin d�placement */
    free(bin);
    return -1;
  } else
    free(dir);                  /* fin d�placement */
  
  switch (pid = vfork()) {      /* fork to create new process */
  case -1:                      /* error */
    warn("CGI execve: vfork() failed");
    return -1;
    break;
  case 0:                       /* child */
    /* �xecution du CGI */
    warnx("CGI execve(\"%s\",{\"%s\", \"%s\"},\
 {\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\"}) in dir \"%s\"",
          argv[0], argv[0], argv[1], envp[0], envp[1],
          envp[2], envp[3], envp[4], envp[5], dir);
    
    execve(argv[0], argv, envp); /* ex�cution du CGI */
    warn("CGI execve: execve returned");

    /* Ici, on met un status incongru pour pouvoir savoir si le CGI a
     * fonctionn� */
    _exit(123456);
    break;                      /* NOTREACHED */
  default:                      /* parent */
    warnx("CGI execve: parent forked");
    break;
  }
  free(bin);
    
  /* wait for exit of child and report status */
  warnx("CGI execve: waiting for child");
  if (waitpid(pid, &status, 0) != pid)
    warn("CGI execve: waitpid failed");
  else if (WIFEXITED(status)) { /* proc terminated normally */
    /* on imprime le code de retour du CGI */
    if (status == 123456)
      return -1;                /* CGI non ex�cut� */
    else                        /* finish "normal" */
      warnx("CGI execve: finished (exit: %d)", WEXITSTATUS(status));
  } if (WIFSIGNALED(status)) { /* process terminated with signal. */
    /* on imprime le signal qui a caus� la fin du CGI */
    warnx("CGI execve: terminated with signal %d", WTERMSIG(status));
    /* il faudrait trouver un moyen que les CGI ne fassent pas de core
       dumps */
    if (WCOREDUMP(status)) /* CGI dumped core! */
      warnx("CGI execve: process core dump!");
  } 

  return 0;
  
} /* fin writeCGI() */

/***********************************************************************
 * �crit une r�ponse appropri� au code HTTP sur clientfd
 *
 * Attention, certains codes ne sont pas implant�s et imprimeront un 500
 * si appel�s.
 * Les explications des codes proviennent du RFC 2616.
 **********************************************************************/
int writeCode(int clientfd, int code) {

  char *title, *body;
  switch (code) {               /* interpr�tation selon RFC 2616 */
  case 400:                     /* Bad Request */
    body = "The request could not be understood by the server due\
 to malformed syntax. The client SHOULD NOT repeat the request without\
 modifications.";
    switch (clientVersion) {
    case HTTP_1_1:
      title = "Bad HTTP/1.1 Request";
      break;
    case HTTP_1_0:
      title = "Bad HTTP/1.0 Request";
      break;
    default:
      title = "Bad HTTP/0.9 Request";
      break;
    }
    break;
  case 403:                     /* Forbidden */
    title = "Forbidden";
    body = "You are not allowed to access this resource.";
    break;
  case 404:                     /* Not Found */
    title = "File not found";
    body = "The requested file was not found on server.";
    break;
  case 501:                     /* Not Implemented */
    title = "Internal server error";
    body = "The server does not support the functionality required\
 to fulfill the request.";
    break;
  /* case 100: */                     /* Continue */
  /* case 101: */                     /* Switching Protocols */
  /* case 200: */                     /* OK */
  /* case 201: */                     /* Created */
  /* case 202: */                     /* Accepted */
  /* case 203: */                     /* Non-Authoritative Information */
  /* case 204: */                     /* No Content */
  /* case 205: */                     /* Reset Content */
  /* case 206: */                     /* Partial Content */
  /* case 300: */                     /* Multiple Choices */
  /* case 301: */                     /* Moved Permanently */
  /* case 302: */                     /* Found */
  /* case 303: */                     /* See Other */
  /* case 304: */                     /* Not Modified */
  /* case 305: */                     /* Use Proxy */
  /* case 307: */                     /* Temporary Redirect */
  /* case 401: */                     /* Unauthorized */
  /* case 402: */                     /* Payment Required */
  /* case 405: */                     /* Method Not Allowed */
  /* case 406: */                     /* Not Acceptable */
  /* case 407: */                     /* Proxy Authentication Required */
  /* case 408: */                     /* Request Time-out */
  /* case 409: */                     /* Conflict */
  /* case 410: */                     /* Gone */
  /* case 411: */                     /* Length Required */
  /* case 412: */                     /* Precondition Failed */
  /* case 413: */                     /* Request Entity Too Large */
  /* case 414: */                     /* Request-URI Too Large */
  /* case 415: */                     /* Unsupported Media Type */
  /* case 416: */                     /* Requested range not satisfiable */
  /* case 417: */                     /* Expectation Failed */
  /* case 502: */                     /* Bad Gateway */
  /* case 503: */                     /* Service Unavailable */
  /* case 504: */                     /* Gateway Time-out */
    /* case 505: */                   /* HTTP Version not supported */
    /* case 500: */                   /* Internal Server Error */
  default:
    title = "Internal server error";
    body = "An internal server error occured. Please contact the\
 system administrator.";
    break;
  }
  warnx("writeCode: %d", code);
  
  if (writeHtmlPage(clientfd, title, body) < 0 ) return -1;
  else return 0;

} /* fin writeCode() */

/***********************************************************************
 * �crit une r�ponse appropri� au code HTTP sur clientfd, header et page
 *
 * Appelle writeHttpHeader() writeCode()
 **********************************************************************/
inline int writeErrorCode(int clientfd, int code) {

  warnx("writeErrorCode: %d", code);
  if ( (writeHttpHeader(clientfd, NULL, code, NULL) < 0) ||
       (writeCode(clientfd, code) < 0))
    return -1;
  else return 0;

} /* fin writeErrorCode() */


#ifdef HOSTFILE
/***********************************************************************
 * �crit les stats sur le serveur sur le fichier HOSTFILE
 **********************************************************************/
void writeHostfile(char* hostname, int port) {

  int hostfile = open(HOSTFILE, O_WRONLY | O_CREAT | O_TRUNC, 0644);
  char portStr[BUFSIZ];

  snprintf(portStr, BUFSIZ, "%d", port);
  
  if (hostfile > 0) {           /* open successful */
    if ( (write(hostfile, hostname, strlen(hostname)) < 0) ||
         (write(hostfile, ":", 1) < 0) ||
         (write(hostfile, portStr, strlen(portStr)) < 0) )
                                /* error in write */
      warn("error while writing to hostfile: \"%s\"", HOSTFILE);
    else warnx("wrote successfully to hostfile: \"%s\": \"%s:%s\"",
               HOSTFILE, hostname, portStr);
    close(hostfile);
  } else
    warn("error opening hostfile: %s", HOSTFILE); /* open failed */
  
}
#endif
