////////////////////////////////////////////////////////////////////////
// Module de communication HTTP/0.9 (parall�le)
////////////////////////////////////////////////////////////////////////
// @author The AnarCat
// @version $Id: httprequest.java,v 0.5 2000-04-12 13:40:18-04 spidey Exp $
////////////////////////////////////////////////////////////////////////

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

////////////////////////////////////////////////////////////////////////
// Requ�te HTTP 0.9 (en parall�le)
////////////////////////////////////////////////////////////////////////
class MultiHttpRequest extends HttpRequest 
    implements Runnable {
    
    private TextArea output;
    private Button but;
    
    public MultiHttpRequest (String hostname, int port)
        throws IOException, UnknownHostException {
        
        super(hostname, port);
        output = new TextArea(); // �vitons les null pointers
        
    }

    public MultiHttpRequest (String hostname,
                             int port,
                             TextArea output, Button send)
        throws IOException, UnknownHostException {
        
        super(hostname, port);
        this.output = output;
        this.but = send;

    }
    
    public void run() { 
        
        try {
            BufferedReader buffer =
                new BufferedReader(new InputStreamReader(sock.getInputStream()));
            output.setText("");
            output.append(buffer.readLine());
            sock.close();
            if (but != null) but.setEnabled(true);
            
        } catch (Exception exc) {
            output.append("An error occurred: " + exc.getMessage());
        }
    }

}
