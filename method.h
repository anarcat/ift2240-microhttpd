/****************************-*-C-*-***********************************
 * $Id: method.h,v 2.3 2000-04-11 22:33:27-04 spidey Exp spidey $
 **********************************************************************
 * D�claration des m�thodes HTTP/1.1
 **********************************************************************
 *   Copyright (C)1999-2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef _METHOD_H
#define _METHOD_H

#include <sys/stat.h>

/**
 * NB: Seulement head et get sont implant�es
 **/

/***********************************************************************
 * Implantation de la m�thode HEAD
 *
 * Appelle writeHttpHeader() avec le code et stat donn�.
 *
 * Retourne -1 si une erreur s'est produite dans writeHttpHeader(), 0
 * sinon.
 **********************************************************************/
int headRequest(int clientfd,   /* le descripteur sur lequel �crire */
                char *path, 
                int code,       /* le code de r�ponse */
                struct stat *fstats, /* le r�sultat d'un stat(2) */
                char* query_string);

/****************************************************************************
 * Implantation de la m�thode GET
 *
 * Appelle writeFile(), writeDir() ou writeCGI() si le code est 2xx et
 * path != NULL. Appelle writeCode() sinon ou si une erreur s'est produite.
 *
 * Retourne 0 si tout va bien ou -1 si une erreur en �criture s'est produite
 ***************************************************************************/
int getRequest(int clientfd,
               char* url, 
               char* path,
               int code,       /* le code de r�ponse */
               struct stat *fstats, /* le r�sultat d'un stat(2) */
               char* query_string);

#endif
